package com.aef.android.astronote.presentation.astro_details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.aef.android.astronote.R;
import com.aef.android.astronote.base.BaseActivity;
import com.aef.android.astronote.base.BaseThemedActivity;
import com.aef.android.astronote.data.model.AstroObject;
import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.data.model.BarlowLens;
import com.aef.android.astronote.data.model.CCDMatrix;
import com.aef.android.astronote.data.model.FocusGear;
import com.aef.android.astronote.data.model.Ocular;
import com.aef.android.astronote.data.model.Person;
import com.aef.android.astronote.data.model.PhotoCamera;
import com.aef.android.astronote.data.model.Telescope;
import com.aef.android.astronote.presentation.create.CreateActivity;
import com.aef.android.astronote.presentation.create.adapter.ChipsAdapter;
import com.aef.android.astronote.presentation.main.BaseRecordListFragment;
import com.aef.android.astronote.utils.DateFormatHelper;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by aef on 09.01.2018.
 */

public class AstroDetailsActivity extends BaseThemedActivity implements OnMapReadyCallback {

    private static final String KEY_DATA = "KEY_DATA";

    @BindView(R.id.map_view)
    MapView mapView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.date_tv)
    TextView dateTextView;
    @BindView(R.id.weather_tv)
    TextView weatherTextView;

    @BindView(R.id.telescopes_tv)
    TextView telescopeTextView;
    @BindView(R.id.cameras_tv)
    TextView cameraTextView;
    @BindView(R.id.matrixes_tv)
    TextView matrixTextView;
    @BindView(R.id.people_tv)
    TextView peopleTextView;
    @BindView(R.id.objects_tv)
    TextView objectsTextView;
    @BindView(R.id.oculars_tv)
    TextView ocularTextView;
    @BindView(R.id.barlow_tv)
    TextView barlowLensTextView;
    @BindView(R.id.focus_gear_tv)
    TextView focusGearTextView;

    @BindView(R.id.telescopes_rv)
    RecyclerView telescopesRv;
    @BindView(R.id.cameras_rv)
    RecyclerView camerasRv;
    @BindView(R.id.matrixes_rv)
    RecyclerView matrixesRv;
    @BindView(R.id.people_rv)
    RecyclerView peopleRv;
    @BindView(R.id.objects_rv)
    RecyclerView objectsRv;
    @BindView(R.id.oculars_rv)
    RecyclerView ocularRv;
    @BindView(R.id.barlow_rv)
    RecyclerView barlowRv;
    @BindView(R.id.focus_gear_rv)
    RecyclerView focusGearRv;

    private AstroRecord astroRecord;
    private GoogleMap googleMap;

    private ChipsAdapter<Telescope> telescopeChipsAdapter;
    private ChipsAdapter<PhotoCamera> cameraChipsAdapter;
    private ChipsAdapter<CCDMatrix> matrixChipsAdapter;
    private ChipsAdapter<Person> peopleChipsAdapter;
    private ChipsAdapter<AstroObject> astroObjectChipsAdapter;
    private ChipsAdapter<Ocular> ocularChipsAdapter;
    private ChipsAdapter<BarlowLens> barlowLensChipsAdapter;
    private ChipsAdapter<FocusGear> focusGearChipsAdapter;

    public static void start(Context context, AstroRecord astroRecord) {
        Intent intent = new Intent(context, AstroDetailsActivity.class);
        intent.putExtra(KEY_DATA, astroRecord);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        astroRecord = getIntent().getParcelableExtra(KEY_DATA);
        fillData();

        setSupportActionBar(toolbar);
        setTitle(null);
        setHasBackButton();
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    private void fillData() {
        dateTextView.setText(String.format("%s %s - %s %s", DateFormatHelper.getDate(astroRecord.getStartDate()),
                DateFormatHelper.getTime(astroRecord.getStartTime()), DateFormatHelper.getDate(astroRecord.getEndDate()),
                DateFormatHelper.getTime(astroRecord.getEndTime())));
        String weather = "";
        if (!TextUtils.isEmpty(astroRecord.getWeatherDescription())) {
            weather += astroRecord.getWeatherDescription();
        }
        if (astroRecord.getWeatherPressure() != 0) {
            if (!TextUtils.isEmpty(weather)) {
                weather += "\n";
            }
            weather += "Давление: " + astroRecord.getWeatherPressure();
        }
        if (!TextUtils.isEmpty(astroRecord.getWeatherTemperature())) {
            if (!TextUtils.isEmpty(weather)) {
                weather += "\n";
            }
            weather += "Температура: " + astroRecord.getWeatherTemperature();
        }
        if (TextUtils.isEmpty(weather)) {
            weatherTextView.setVisibility(View.GONE);
        } else {
            weatherTextView.setText(weather);
        }

        //init adapters
        SpacingItemDecoration itemDecoration = new SpacingItemDecoration(getResources().getDimensionPixelSize(R.dimen.margin_xs),
                getResources().getDimensionPixelSize(R.dimen.margin_xs));

        //telescopes
        if (astroRecord.getTelescopes() != null && !astroRecord.getTelescopes().isEmpty()) {
            telescopeTextView.setVisibility(View.VISIBLE);
            telescopesRv.setVisibility(View.VISIBLE);
            telescopesRv.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
            telescopesRv.addItemDecoration(itemDecoration);
            telescopeChipsAdapter = new ChipsAdapter<>(astroRecord.getTelescopes());
            telescopesRv.setAdapter(telescopeChipsAdapter);
        }

        //cameras
        if (astroRecord.getPhotoCameras() != null && !astroRecord.getPhotoCameras().isEmpty()) {
            camerasRv.setVisibility(View.VISIBLE);
            cameraTextView.setVisibility(View.VISIBLE);
            camerasRv.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
            camerasRv.addItemDecoration(itemDecoration);
            cameraChipsAdapter = new ChipsAdapter<>(astroRecord.getPhotoCameras());
            camerasRv.setAdapter(cameraChipsAdapter);
        }

        //matrixes
        if (astroRecord.getCcdMatrixList() != null && !astroRecord.getCcdMatrixList().isEmpty()) {
            matrixesRv.setVisibility(View.VISIBLE);
            matrixTextView.setVisibility(View.VISIBLE);
            matrixesRv.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
            matrixesRv.addItemDecoration(itemDecoration);
            matrixChipsAdapter = new ChipsAdapter<>(astroRecord.getCcdMatrixList());
            matrixesRv.setAdapter(matrixChipsAdapter);
        }

        //oculars
        if (astroRecord.getOculars() != null && !astroRecord.getOculars().isEmpty()) {
            ocularRv.setVisibility(View.VISIBLE);
            ocularTextView.setVisibility(View.VISIBLE);
            ocularRv.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
            ocularRv.addItemDecoration(itemDecoration);
            ocularChipsAdapter = new ChipsAdapter<>(astroRecord.getOculars());
            ocularRv.setAdapter(ocularChipsAdapter);
        }

        //barlow
        if (astroRecord.getBarlowLensList() != null && !astroRecord.getBarlowLensList().isEmpty()) {
            barlowLensTextView.setVisibility(View.VISIBLE);
            barlowRv.setVisibility(View.VISIBLE);
            barlowRv.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
            barlowRv.addItemDecoration(itemDecoration);
            barlowLensChipsAdapter = new ChipsAdapter<>(astroRecord.getBarlowLensList());
            barlowRv.setAdapter(barlowLensChipsAdapter);
        }

        //focus gear
        if (astroRecord.getFocusGears() != null && !astroRecord.getFocusGears().isEmpty()) {
            focusGearRv.setVisibility(View.VISIBLE);
            focusGearTextView.setVisibility(View.VISIBLE);
            focusGearRv.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
            focusGearRv.addItemDecoration(itemDecoration);
            focusGearChipsAdapter = new ChipsAdapter<>(astroRecord.getFocusGears());
            focusGearRv.setAdapter(focusGearChipsAdapter);
        }

        //people
        if (astroRecord.getPeopleList() != null && !astroRecord.getPeopleList().isEmpty()) {
            peopleRv.setVisibility(View.VISIBLE);
            peopleTextView.setVisibility(View.VISIBLE);
            peopleRv.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
            peopleRv.addItemDecoration(itemDecoration);
            peopleChipsAdapter = new ChipsAdapter<>(astroRecord.getPeopleList());
            peopleRv.setAdapter(peopleChipsAdapter);
        }

        //astro objects
        if (astroRecord.getAstroObjectList() != null && !astroRecord.getAstroObjectList().isEmpty()) {
            objectsRv.setVisibility(View.VISIBLE);
            objectsTextView.setVisibility(View.VISIBLE);
            objectsRv.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
            objectsRv.addItemDecoration(itemDecoration);
            astroObjectChipsAdapter = new ChipsAdapter<>(astroRecord.getAstroObjectList());
            objectsRv.setAdapter(astroObjectChipsAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        double latitude = CreateActivity.DEFAULT_LATITUDE;
        double longitude = CreateActivity.DEFAULT_LONGITUDE;
        if (astroRecord.getLatitude() != 0 && astroRecord.getLongitude() != 0) {
            latitude = astroRecord.getLatitude();
            longitude = astroRecord.getLongitude();
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(new LatLng(latitude, longitude));
            googleMap.addMarker(markerOptions);
        }
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(latitude, longitude), 12));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}
