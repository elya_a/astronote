package com.aef.android.astronote.presentation.create_tool.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.FocusGear;

import butterknife.BindView;

public class CreateFocusGearFragment extends BaseCreateToolFragment<FocusGear> {

    @BindView(R.id.multi_et)
    EditText multiplicityEditText;

    public static CreateFocusGearFragment newInstance(FocusGear focusGear) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_TOOL, focusGear);
        CreateFocusGearFragment fragment = new CreateFocusGearFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_create_focus_gear;
    }

    @Override
    protected FocusGear newTool() {
        return new FocusGear();
    }

    @Override
    protected void initFields(FocusGear tool) {
        multiplicityEditText.setText(String.valueOf(tool.getMultiplicity()));
    }

    @Override
    protected boolean isValid() {
        return !TextUtils.isEmpty(multiplicityEditText.getText().toString());
    }

    @Override
    protected FocusGear getToolModel() {
        tool.setMultiplicity(Double.valueOf(multiplicityEditText.getText().toString()));
        return tool;
    }
}
