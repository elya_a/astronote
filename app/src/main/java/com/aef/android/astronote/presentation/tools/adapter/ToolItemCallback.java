package com.aef.android.astronote.presentation.tools.adapter;

import com.aef.android.astronote.data.model.Tool;

public interface ToolItemCallback {

    void onEditItem(Tool tool);

    void onDeleteItem(Tool tool, int position);
}
