package com.aef.android.astronote.presentation.create;

import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.data.repository.astrorecord.AstroRecordRepositoryImpl;
import com.aef.android.astronote.data.repository.astrorecord.AstroRecordRepository;
import com.aef.android.astronote.data.repository.person.PersonRepository;
import com.aef.android.astronote.data.repository.person.PersonRepositoryImpl;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class CreatePresenter implements CreateContract.Presenter {

    private CreateContract.View view;
    private AstroRecordRepository astroRecordRepository;
    private PersonRepository personRepository;

    public CreatePresenter(CreateContract.View view) {
        this.view = view;
        astroRecordRepository = new AstroRecordRepositoryImpl();
        personRepository = new PersonRepositoryImpl();
    }

    @Override
    public void saveAstroRecord(AstroRecord astroRecord) {
        astroRecordRepository.saveAstroRecord(astroRecord)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::recordSaved, throwable -> {
                    throwable.printStackTrace();
                    view.onError();
                });
    }

    @Override
    public void loadPeople() {
        personRepository.getAllPersonsNames()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::showPeople, Throwable::printStackTrace);
    }
}
