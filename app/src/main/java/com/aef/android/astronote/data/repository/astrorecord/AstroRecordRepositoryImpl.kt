package com.aef.android.astronote.data.repository.astrorecord

import com.aef.android.astronote.data.database.AstroRecordTableModel
import com.aef.android.astronote.data.model.AstroRecord
import com.aef.android.astronote.data.repository.BaseRepository
import com.aef.android.astronote.data.repository.person.PersonRepositoryImpl
import com.aef.android.astronote.data.repository.tools.ToolsRepository

import java.sql.SQLException
import java.util.ArrayList

import io.reactivex.Observable


class AstroRecordRepositoryImpl : BaseRepository(), AstroRecordRepository {

    override fun saveAstroRecord(astroRecord: AstroRecord): Observable<AstroRecord> {
        return Observable.create { subscriber ->
            try {
                val astroRecordTableModel = AstroRecordTableModel(astroRecord)
                val personRepository = PersonRepositoryImpl()
                personRepository.savePersonList(astroRecordTableModel.peopleList)

                dataBaseHelper.astroRecordDao.createOrUpdate(astroRecordTableModel)
                subscriber.onNext(AstroRecord(astroRecordTableModel))
                subscriber.onComplete()
            } catch (e: SQLException) {
                subscriber.onError(e)
            }
        }
    }

    override fun loadAllAstroRecords(): Observable<ArrayList<AstroRecord>> {
        return Observable.create { subscriber ->
            val astroRecords = ArrayList<AstroRecord>()
            val toolsRepository = ToolsRepository()
            try {
                for (tableModel in dataBaseHelper.astroRecordDao.allRecords) {

                    if (tableModel.telescopes != null) {
                        tableModel.telescopeTableModels = toolsRepository.getTelescopesByJsons(tableModel.telescopes)
                    }

                    if (tableModel.photoCameras != null) {
                        tableModel.photoCameraTableModels = toolsRepository.getCamerasByJsons(tableModel.photoCameras)
                    }

                    if (tableModel.ccdMatrixList != null) {
                        tableModel.ccdMatrixTableModels = toolsRepository.getMatrixesByJsons(tableModel.ccdMatrixList)
                    }

                    if (tableModel.barlowLensIdList != null) {
                        tableModel.barlowLensTableModels =
                            toolsRepository.getBarlowLensByJsons(tableModel.barlowLensIdList)
                    }

                    if (tableModel.ocularIdList != null) {
                        tableModel.ocularsTableModels = toolsRepository.getOcularsByJsons(tableModel.ocularIdList)
                    }

                    if (tableModel.focusGearIdList != null) {
                        tableModel.focusGearTableModels =
                            toolsRepository.getFocusGearsByJsons(tableModel.focusGearIdList)
                    }

                    astroRecords.add(AstroRecord(tableModel))
                }
                subscriber.onNext(astroRecords)
                subscriber.onComplete()
            } catch (e: SQLException) {
                subscriber.onError(e)
            }
        }
    }
}
