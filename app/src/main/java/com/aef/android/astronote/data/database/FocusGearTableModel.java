package com.aef.android.astronote.data.database;

import com.aef.android.astronote.data.model.FocusGear;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import static com.aef.android.astronote.data.database.BarlowLensTableModel.FIELD_MULTIPLICITY;
import static com.aef.android.astronote.data.database.FocusGearTableModel.FOCUS_GEAR_TABLE_NAME;

@DatabaseTable(tableName = FOCUS_GEAR_TABLE_NAME)
public class FocusGearTableModel {

    public static final String FOCUS_GEAR_TABLE_NAME = "focus_gears";

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private int id;
    @DatabaseField(columnName = FIELD_MULTIPLICITY)
    private double multiplicity;

    public FocusGearTableModel() {
    }

    public FocusGearTableModel(FocusGear focusGear) {
        id = focusGear.getId();
        multiplicity = focusGear.getMultiplicity();
    }

    public int getId() {
        return id;
    }

    public double getMultiplicity() {
        return multiplicity;
    }
}
