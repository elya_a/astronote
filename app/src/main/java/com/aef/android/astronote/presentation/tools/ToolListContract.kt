package com.aef.android.astronote.presentation.tools

import com.aef.android.astronote.base.Presenter
import com.aef.android.astronote.base.View
import com.aef.android.astronote.data.model.Tool


class ToolListContract {

    interface ToolListView : View{

        fun showToolList(toolArrayList: List<Tool>)
    }

    interface ToolListPresenter : Presenter<ToolListView> {

        fun loadTools()

        fun deleteTool(tool: Tool)
    }
}
