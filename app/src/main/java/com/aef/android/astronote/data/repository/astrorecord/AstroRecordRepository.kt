package com.aef.android.astronote.data.repository.astrorecord

import com.aef.android.astronote.data.model.AstroRecord
import io.reactivex.Observable
import java.util.ArrayList

interface AstroRecordRepository {

    fun saveAstroRecord(astroRecord: AstroRecord): Observable<AstroRecord>

    fun loadAllAstroRecords(): Observable<ArrayList<AstroRecord>>
}