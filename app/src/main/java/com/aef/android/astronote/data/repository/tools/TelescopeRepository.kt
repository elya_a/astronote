package com.aef.android.astronote.data.repository.tools

import com.aef.android.astronote.data.database.TelescopeTableModel
import com.aef.android.astronote.data.model.Telescope
import com.aef.android.astronote.data.repository.BaseRepository
import com.aef.android.astronote.data.repository.tools.BaseToolRepository
import com.google.gson.Gson

import java.sql.SQLException
import java.util.ArrayList

import io.reactivex.Observable

class TelescopeRepository(private val gson: Gson) : BaseRepository(), BaseToolRepository<Telescope> {

    override fun getToolList(): Observable<List<Telescope>> =
        Observable.fromCallable {
            dataBaseHelper.telescopeDao.queryForAll()
                .map { Telescope(it) }
        }

    override fun getToolListByJsons(jsons: List<String>): Observable<List<Telescope>> =
        Observable.fromCallable {
            val telescopeDao = dataBaseHelper.telescopeDao
            jsons.map {
                val telescope = gson.fromJson<Telescope>(it, Telescope::class.java)
                if (telescopeDao.getTableModel(telescope.id) == null) {
                    telescope.id = DEFAULT_ID
                }
                telescope
            }
        }

    override fun saveTool(tool: Telescope): Observable<Telescope> =
        Observable.fromCallable {
            val tableModel = TelescopeTableModel(tool)
            dataBaseHelper.telescopeDao.createOrUpdate(tableModel)
            Telescope(tableModel)
        }


    override fun deleteTool(tool: Telescope): Observable<Telescope> =
        Observable.fromCallable {
            val tableModel = TelescopeTableModel(tool)
            dataBaseHelper.telescopeDao.delete(tableModel)
            Telescope(tableModel)
        }
}
