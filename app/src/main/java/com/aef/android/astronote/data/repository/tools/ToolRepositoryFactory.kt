package com.aef.android.astronote.data.repository.tools

import com.aef.android.astronote.data.model.BarlowLens
import com.aef.android.astronote.data.model.CCDMatrix
import com.aef.android.astronote.data.model.FocusGear
import com.aef.android.astronote.data.model.Ocular
import com.aef.android.astronote.data.model.PhotoCamera
import com.aef.android.astronote.data.model.Telescope
import com.aef.android.astronote.data.model.Tool
import com.aef.android.astronote.data.model.ToolType
import com.aef.android.astronote.data.repository.tools.BaseToolRepository
import com.aef.android.astronote.data.repository.tools.CameraRepository
import com.aef.android.astronote.data.repository.tools.MatrixRepository
import com.aef.android.astronote.data.repository.tools.TelescopeRepository

/**
 * Created by aef on 21.06.2017.
 */

class ToolRepositoryFactory(
    private val cameraRepository: BaseToolRepository<PhotoCamera>,
    private val telescopeRepository: BaseToolRepository<Telescope>,
    private val matrixRepository: BaseToolRepository<CCDMatrix>,
    private val ocularRepository: BaseToolRepository<Ocular>,
    private val barlowLensRepository: BaseToolRepository<BarlowLens>,
    private val focusGearRepository: BaseToolRepository<FocusGear>
) {

    fun getToolRepository(toolType: ToolType): BaseToolRepository<*> =
        when (toolType) {
            ToolType.BARLOW_LENS -> barlowLensRepository
            ToolType.CAMERA -> cameraRepository
            ToolType.FOCUS_GEAR -> focusGearRepository
            ToolType.MATRIX -> matrixRepository
            ToolType.OCULAR -> ocularRepository
            ToolType.TELESCOPE -> telescopeRepository
        }
}
