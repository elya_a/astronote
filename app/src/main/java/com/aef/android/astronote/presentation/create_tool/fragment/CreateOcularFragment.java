package com.aef.android.astronote.presentation.create_tool.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.Ocular;

import butterknife.BindView;


public class CreateOcularFragment extends BaseCreateToolFragment<Ocular> {

    @BindView(R.id.focus_et)
    EditText focusEditText;
    @BindView(R.id.view_field_et)
    EditText viewFieldEditText;

    public static BaseCreateToolFragment newInstance(Ocular tool) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_TOOL, tool);
        CreateOcularFragment fragment = new CreateOcularFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_create_ocular;
    }

    @Override
    protected Ocular newTool() {
        return new Ocular();
    }

    @Override
    protected void initFields(Ocular tool) {
        focusEditText.setText(String.valueOf(tool.getFocus()));
        viewFieldEditText.setText(String.valueOf(tool.getViewField()));
    }

    @Override
    protected boolean isValid() {
        return !TextUtils.isEmpty(focusEditText.getText().toString());
    }

    @Override
    protected Ocular getToolModel() {
        tool.setFocus(Double.valueOf(focusEditText.getText().toString()));
        if (!TextUtils.isEmpty(viewFieldEditText.getText().toString())) {
            tool.setViewField(Double.valueOf(viewFieldEditText.getText().toString()));
        }
        return tool;
    }
}
