package com.aef.android.astronote.presentation.main;

public class PlannedFragment extends BaseRecordListFragment {

    public static PlannedFragment newInstance() {
        return new PlannedFragment();
    }

    @Override
    public boolean isPlanned() {
        return true;
    }
}
