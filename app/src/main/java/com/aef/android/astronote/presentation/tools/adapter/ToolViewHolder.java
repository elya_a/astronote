package com.aef.android.astronote.presentation.tools.adapter;

import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.Tool;

public abstract class ToolViewHolder<T extends Tool> extends RecyclerView.ViewHolder {

    protected T data;
    protected boolean isDark;

    public ToolViewHolder(View itemView) {
        super(itemView);
    }

    public ToolViewHolder(View itemView, ToolItemCallback<T> toolItemCallback) {
        super(itemView);
        if (toolItemCallback != null) {
            itemView.setOnLongClickListener(view -> {
                String[] list = {itemView.getContext().getString(R.string.edit),
                        itemView.getContext().getString(R.string.delete)};
                new AlertDialog.Builder(itemView.getContext())
                        .setItems(list, (dialogInterface, i) -> {
                            switch (i) {
                                case 0:
                                    toolItemCallback.onEditItem(data);
                                    break;
                                case 1:
                                    toolItemCallback.onDeleteItem(data, getAdapterPosition());
                                    break;
                            }
                        })
                        .create()
                        .show();
                return true;
            });
        }
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setDark(boolean dark) {
        isDark = dark;
    }
}
