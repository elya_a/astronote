package com.aef.android.astronote.presentation.create_tool.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.CCDMatrix;

import butterknife.BindView;

public class CreateMatrixFragment extends BaseCreateToolFragment<CCDMatrix> {

    @BindView(R.id.name_et)
    EditText nameEditText;
    @BindView(R.id.mpixels_et)
    EditText mpixelsEditText;
    @BindView(R.id.sensor_et)
    EditText sensorEditText;
    @BindView(R.id.crop_et)
    EditText cropEditText;

    public static CreateMatrixFragment newInstance() {
        return new CreateMatrixFragment();
    }

    public static BaseCreateToolFragment newInstance(CCDMatrix tool) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_TOOL, tool);
        CreateMatrixFragment fragment = new CreateMatrixFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_create_matrix;
    }


    @Override
    protected CCDMatrix newTool() {
        return new CCDMatrix();
    }

    @Override
    protected void initFields(CCDMatrix tool) {
        nameEditText.setText(tool.getName());
        mpixelsEditText.setText(String.valueOf(tool.getMegaPixelCount()));
        sensorEditText.setText(tool.getSensorSize());
        cropEditText.setText(String.valueOf(tool.getCropFactor()));
    }

    @Override
    protected boolean isValid() {
        return !(TextUtils.isEmpty(nameEditText.getText()));
    }

    @Override
    public CCDMatrix getToolModel() {
        CCDMatrix matrix = tool;
        matrix.setName(nameEditText.getText().toString());
        if (!TextUtils.isEmpty(mpixelsEditText.getText().toString())) {
            matrix.setMegaPixelCount(Double.valueOf(mpixelsEditText.getText().toString()));
        }
        if (!TextUtils.isEmpty(sensorEditText.getText().toString())) {
            matrix.setSensorSize(sensorEditText.getText().toString());
        }
        if (!TextUtils.isEmpty(cropEditText.getText().toString())) {
            matrix.setCropFactor(Double.valueOf(cropEditText.getText().toString()));
        }
        return matrix;
    }
}
