package com.aef.android.astronote.data.model

import android.os.Parcelable

import androidx.annotation.IntDef

import com.aef.android.astronote.presentation.create.adapter.ChipItem

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

abstract class Tool(
    val toolType: ToolType
) : Parcelable, ChipItem

enum class ToolType {
    CAMERA,
    TELESCOPE,
    MATRIX, OCULAR,
    BARLOW_LENS,
    FOCUS_GEAR
}