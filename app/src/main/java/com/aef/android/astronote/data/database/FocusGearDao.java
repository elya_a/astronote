package com.aef.android.astronote.data.database;

import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;


public class FocusGearDao extends BaseDao<FocusGearTableModel> {

    protected FocusGearDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, FocusGearTableModel.class);
    }
}
