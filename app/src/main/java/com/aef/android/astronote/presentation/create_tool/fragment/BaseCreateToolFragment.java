package com.aef.android.astronote.presentation.create_tool.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;

import com.aef.android.astronote.base.BaseFragment;
import com.aef.android.astronote.data.model.Tool;

import butterknife.ButterKnife;

public abstract class BaseCreateToolFragment<T extends Tool> extends BaseFragment {

    public static final String KEY_TOOL = "KEY_TOOL";
    protected T tool;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutRes(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tool = getArguments().getParcelable(KEY_TOOL);
        if (tool != null) {
            initFields(tool);
        } else {
            tool = newTool();
        }
    }

    protected abstract T newTool();

    protected abstract void initFields(T tool);

    @Nullable
    public T getTool() {
        if (isValid()) {
            return getToolModel();
        }
        return null;
    }

    protected abstract boolean isValid();

    protected abstract T getToolModel();
}
