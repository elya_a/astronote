package com.aef.android.astronote.presentation.main;

import com.aef.android.astronote.base.BaseFragmentInteraction;
import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.data.model.Tool;

/**
 * Created by aef on 09.09.2017.
 */

public interface MainActivityFragmentInteraction extends BaseFragmentInteraction {

    void showEditToolScreen(Tool tool);

    void showEditAstroRecordScreen(AstroRecord astroRecord);
}
