package com.aef.android.astronote.presentation.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aef.android.astronote.R;
import com.aef.android.astronote.base.BaseInteractionFragment;
import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.presentation.astro_details.AstroDetailsActivity;
import com.aef.android.astronote.presentation.main.adapters.AstroRecordListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseRecordListFragment extends BaseInteractionFragment<MainActivityFragmentInteraction>
        implements MainContract.View, AstroRecordListAdapter.AstroCallback {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private MainContract.Presenter presenter;
    private AstroRecordListAdapter recordListAdapter;
    private ArrayList<AstroRecord> astroRecords;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setPresenter(new MainPresenter(this));
        presenter.loadAstroRecords(isPlanned());

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showAstroRecords(ArrayList<AstroRecord> astroRecords) {
        this.astroRecords = astroRecords;
        progressBar.setVisibility(View.GONE);
        recordListAdapter = new AstroRecordListAdapter(astroRecords, this);
        recyclerView.setAdapter(recordListAdapter);
    }

    @Override
    public void onError() {
        progressBar.setVisibility(View.GONE);
    }

    public void astroRecordeCreated(AstroRecord astroRecord) {
        if (astroRecords.contains(astroRecord)) {
            astroRecords.set(astroRecords.indexOf(astroRecord), astroRecord);
        } else {
            astroRecords.add(astroRecord);
        }
        recordListAdapter.notifyItemInserted(astroRecords.indexOf(astroRecord));
    }

    public abstract boolean isPlanned();

    @Override
    public void showAstroRecord(AstroRecord astroRecord) {
        AstroDetailsActivity.start(getActivity(), astroRecord);
    }

    @Override
    public void editAstroRecord(AstroRecord astroRecord) {
        fragmentInteractionCallback.showEditAstroRecordScreen(astroRecord);
    }

    @Override
    public void deleteAstroRecord(AstroRecord astroRecord) {

    }
}
