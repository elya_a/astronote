package com.aef.android.astronote.presentation.create.adapter

interface ChipsListener<T : ChipItem> {

    fun closeChip(chip: T)
}
