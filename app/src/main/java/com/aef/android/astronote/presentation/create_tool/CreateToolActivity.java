package com.aef.android.astronote.presentation.create_tool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.aef.android.astronote.R;
import com.aef.android.astronote.base.BaseThemedActivity;
import com.aef.android.astronote.data.model.BarlowLens;
import com.aef.android.astronote.data.model.CCDMatrix;
import com.aef.android.astronote.data.model.FocusGear;
import com.aef.android.astronote.data.model.Ocular;
import com.aef.android.astronote.data.model.PhotoCamera;
import com.aef.android.astronote.data.model.Telescope;
import com.aef.android.astronote.data.model.Tool;
import com.aef.android.astronote.presentation.create_tool.fragment.BaseCreateToolFragment;
import com.aef.android.astronote.presentation.create_tool.fragment.CreateBarlowFragment;
import com.aef.android.astronote.presentation.create_tool.fragment.CreateCameraFragment;
import com.aef.android.astronote.presentation.create_tool.fragment.CreateFocusGearFragment;
import com.aef.android.astronote.presentation.create_tool.fragment.CreateMatrixFragment;
import com.aef.android.astronote.presentation.create_tool.fragment.CreateOcularFragment;
import com.aef.android.astronote.presentation.create_tool.fragment.CreateTelescopeFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateToolActivity extends BaseThemedActivity implements CreateToolContract.View {

    private static final String KEY_TOOL_TYPE = "KEY_TOOL_TYPE";
    public static final String KEY_TOOL = "KEY_TOOL";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private CreateToolContract.Presenter presenter;
    private
    @Tool.Type
    int toolType;

    public static void startForResult(Activity activity, int requestCode, @Tool.Type int toolType) {
        Intent intent = new Intent(activity, CreateToolActivity.class);
        intent.putExtra(KEY_TOOL_TYPE, toolType);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void startForResult(Activity activity, int requestCode, Tool tool, @Tool.Type int toolType) {
        Intent intent = new Intent(activity, CreateToolActivity.class);
        intent.putExtra(KEY_TOOL_TYPE, toolType);
        intent.putExtra(KEY_TOOL, tool);
        activity.startActivityForResult(intent, requestCode);
    }

    @SuppressWarnings("WrongConstant")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tool);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        setHasBackButton();

        setPresenter(new CreateToolPresenter(this));
        toolType = getIntent().getIntExtra(KEY_TOOL_TYPE, Tool.Companion.getCAMERA());
        Tool tool = getIntent().getParcelableExtra(KEY_TOOL);
        switch (toolType) {
            case Tool.Companion.getCAMERA():
                setTitle(getString(R.string.camera));
                showFragment(R.id.content, CreateCameraFragment.newInstance((PhotoCamera) tool), false);
                break;
            case Tool.Companion.getMATRIX():
                setTitle(R.string.matrix);
                showFragment(R.id.content, CreateMatrixFragment.newInstance((CCDMatrix) tool), false);
                break;
            case Tool.Companion.getTELESCOPE():
                setTitle(getString(R.string.telescope));
                showFragment(R.id.content, CreateTelescopeFragment.newInstance((Telescope) tool), false);
                break;
            case Tool.Companion.getBARLOW_LENS():
                setTitle(getString(R.string.barlow_lens));
                showFragment(R.id.content, CreateBarlowFragment.newInstance((BarlowLens) tool), false);
                break;
            case Tool.Companion.getFOCUS_GEAR():
                setTitle(getString(R.string.focus_gear));
                showFragment(R.id.content, CreateFocusGearFragment.newInstance((FocusGear) tool), false);
                break;
            case Tool.Companion.getOCULAR():
                setTitle(R.string.ocular);
                showFragment(R.id.content, CreateOcularFragment.newInstance((Ocular) tool), false);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        initDrawableColor(menu.findItem(R.id.action_done).getIcon(), android.R.color.white);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_done) {
            save();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void save() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment instanceof BaseCreateToolFragment) {
            switch (toolType) {
                case Tool.Companion.getCAMERA():
                    PhotoCamera photoCamera = ((BaseCreateToolFragment<PhotoCamera>) fragment).getTool();
                    if (photoCamera != null) {
                        presenter.saveCamera(photoCamera);
                    }
                    break;
                case Tool.Companion.getMATRIX():
                    CCDMatrix matrix = ((BaseCreateToolFragment<CCDMatrix>) fragment).getTool();
                    if (matrix != null) {
                        presenter.saveMatrix(matrix);
                    }
                    break;
                case Tool.Companion.getTELESCOPE():
                    Telescope telescope = ((BaseCreateToolFragment<Telescope>) fragment).getTool();
                    if (telescope != null) {
                        presenter.saveTelescope(telescope);
                    }
                    break;
                case Tool.Companion.getBARLOW_LENS():
                    BarlowLens barlowLens = ((BaseCreateToolFragment<BarlowLens>) fragment).getTool();
                    if (barlowLens != null) {
                        presenter.saveBarlowLens(barlowLens);
                    }
                    break;
                case Tool.Companion.getFOCUS_GEAR():
                    FocusGear focusGear = ((BaseCreateToolFragment<FocusGear>) fragment).getTool();
                    if (focusGear != null) {
                        presenter.saveFocusGear(focusGear);
                    }
                    break;
                case Tool.Companion.getOCULAR():
                    Ocular ocular = ((BaseCreateToolFragment<Ocular>) fragment).getTool();
                    if (ocular != null) {
                        presenter.saveOcular(ocular);
                    }
                    break;
            }
        }
    }

    @Override
    public void toolSaved(Tool tool) {
        Intent intent = new Intent();
        intent.putExtra(KEY_TOOL, tool);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void setPresenter(CreateToolContract.Presenter presenter) {
        this.presenter = presenter;
    }
}
