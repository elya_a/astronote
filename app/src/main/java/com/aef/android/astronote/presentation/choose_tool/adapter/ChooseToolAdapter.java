package com.aef.android.astronote.presentation.choose_tool.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.Tool;

import java.util.ArrayList;

public class ChooseToolAdapter<T extends Tool> extends RecyclerView.Adapter<ChooseToolViewHolder> {

    private ArrayList<T> toolArrayList;
    private ArrayList<T> selectedToolList;
    private boolean isDarkTheme;

    public ChooseToolAdapter(ArrayList<T> toolArrayList, ArrayList<T> selectedList, boolean isDarkTheme) {
        this.toolArrayList = toolArrayList;
        this.selectedToolList = selectedList;
        this.isDarkTheme = isDarkTheme;
    }

    @Override
    public ChooseToolViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_short_tool_item, parent, false);
        return new ChooseToolViewHolder<T>(view, selectedToolList, isDarkTheme);
    }

    @Override
    public void onBindViewHolder(ChooseToolViewHolder holder, int position) {
        holder.setData(toolArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return toolArrayList.size();
    }

    public ArrayList<T> getSelectedToolList() {
        return selectedToolList;
    }
}
