package com.aef.android.astronote.presentation.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.aef.android.astronote.R;
import com.aef.android.astronote.base.BaseThemedActivity;
import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.data.model.Tool;
import com.aef.android.astronote.presentation.create.CreateActivity;
import com.aef.android.astronote.presentation.create.CreateFragment;
import com.aef.android.astronote.presentation.create_tool.CreateToolActivity;
import com.aef.android.astronote.presentation.tools.ToolListFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseThemedActivity implements NavigationView.OnNavigationItemSelectedListener, MainActivityFragmentInteraction {

    private static final int CREATE_ASTRO_NOTE_REQUEST_CODE = 1;
    private static final String KEY_SELECTED_ITEM = "KEY_SELECTED_ITEM";
    private static final int CREATE_TOOL_REQUEST_CODE = 2;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.create_btn)
    FloatingActionButton createFab;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer)
    DrawerLayout drawerLayout;
    private int selectedItemPos;
    private int selectedItemId;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setHasBackButton(R.drawable.ic_menu_white_24dp);
        showFragment(MainFragment.newInstance());

        if (isDarkTheme) {
            createFab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorPrimary_Dark)));
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.ic_add_white_48dp).mutate();
            drawable.setColorFilter(ContextCompat.getColor(this, R.color.red), PorterDuff.Mode.SRC_ATOP);
            createFab.setImageDrawable(drawable);
            navigationView.setItemIconTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.red)));

        }

        selectedItemPos = 0;

        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState != null) {
            selectedItemPos = savedInstanceState.getInt(KEY_SELECTED_ITEM);
            //navigationView.getMenu().getItem(3).setCheckable(true);
            onNavigationItemSelected(navigationView.getMenu().getItem(selectedItemPos));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(Gravity.LEFT);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_SELECTED_ITEM, selectedItemPos);
    }

    private void showFragment(Fragment fragment) {
        showFragment(R.id.content, fragment, false);
    }

    @OnClick(R.id.create_btn)
    void onFabClick() {
        switch (selectedItemId) {
            case R.id.action_note:
            default:
                CreateActivity.startForResult(this, CREATE_ASTRO_NOTE_REQUEST_CODE);
                break;
            case R.id.action_camera:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, Tool.Companion.getCAMERA());
                break;
            case R.id.action_matrix:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, Tool.Companion.getMATRIX());
                break;
            case R.id.action_telescopes:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, Tool.Companion.getTELESCOPE());
                break;
            case R.id.action_oculars:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, Tool.Companion.getOCULAR());
                break;
            case R.id.action_barlow:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, Tool.Companion.getBARLOW_LENS());
                break;
            case R.id.action_focus_gear:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, Tool.Companion.getFOCUS_GEAR());
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CREATE_ASTRO_NOTE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        AstroRecord astroRecord = data.getParcelableExtra(CreateFragment.KEY_ASTRO_RECORD);
                        if (astroRecord != null) {
                            Fragment fragment = getSupportFragmentManager().findFragmentByTag(BaseRecordListFragment.class.getSimpleName());
                            if (fragment != null && fragment instanceof BaseRecordListFragment) {
                                ((BaseRecordListFragment)fragment).astroRecordeCreated(astroRecord);
                            }
                        }
                    }
                }
                break;
            case CREATE_TOOL_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    Tool tool = data.getParcelableExtra(CreateToolActivity.KEY_TOOL);
                    if (tool != null) {
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag(ToolListFragment.class.getSimpleName());
                        if (fragment != null && fragment instanceof ToolListFragment) {
                            ((ToolListFragment) fragment).updateTool(tool);
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        selectedItemId = item.getItemId();
        setTitle(item.getTitle());
        switch (item.getItemId()) {
            case R.id.action_note:
                selectedItemPos = 0;
                setTitle(R.string.app_name);
                showFragment(MainFragment.newInstance());
                break;
            case R.id.action_plan:
                selectedItemPos = 1;
                showFragment(PlannedFragment.newInstance());
                break;
            case R.id.action_telescopes:
                selectedItemPos = 2;
                showFragment(ToolListFragment.newInstance(Tool.Companion.getTELESCOPE()));
                break;
            case R.id.action_camera:
                selectedItemPos = 3;
                showFragment(ToolListFragment.newInstance(Tool.Companion.getCAMERA()));
                break;
            case R.id.action_matrix:
                selectedItemPos = 4;
                showFragment(ToolListFragment.newInstance(Tool.Companion.getMATRIX()));
                break;
            case R.id.action_oculars:
                selectedItemPos = 5;
                showFragment(ToolListFragment.newInstance(Tool.Companion.getOCULAR()));
                break;
            case R.id.action_barlow:
                selectedItemPos = 6;
                showFragment(ToolListFragment.newInstance(Tool.Companion.getBARLOW_LENS()));
                break;
            case R.id.action_focus_gear:
                selectedItemPos = 7;
                showFragment(ToolListFragment.newInstance(Tool.Companion.getFOCUS_GEAR()));
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void showEditToolScreen(Tool tool) {
        switch (selectedItemId) {
            case R.id.action_camera:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, tool, Tool.Companion.getCAMERA());
                break;
            case R.id.action_matrix:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, tool, Tool.Companion.getMATRIX());
                break;
            case R.id.action_telescopes:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, tool, Tool.Companion.getTELESCOPE());
                break;
            case R.id.action_oculars:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, tool, Tool.Companion.getOCULAR());
                break;
            case R.id.action_barlow:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, tool, Tool.Companion.getBARLOW_LENS());
                break;
            case R.id.action_focus_gear:
                CreateToolActivity.startForResult(this, CREATE_TOOL_REQUEST_CODE, tool, Tool.Companion.getFOCUS_GEAR());
                break;
        }
    }

    @Override
    public void showEditAstroRecordScreen(AstroRecord astroRecord) {
        CreateActivity.startForResult(this, CREATE_ASTRO_NOTE_REQUEST_CODE, astroRecord);
    }
}
