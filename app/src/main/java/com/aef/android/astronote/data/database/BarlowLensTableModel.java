package com.aef.android.astronote.data.database;


import com.aef.android.astronote.data.model.BarlowLens;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import static com.aef.android.astronote.data.database.BarlowLensTableModel.BARLOW_TABLE_NAME;

@DatabaseTable(tableName = BARLOW_TABLE_NAME)
public class BarlowLensTableModel {
    public static final String BARLOW_TABLE_NAME = "barlow_lens";
    public static final String FIELD_MULTIPLICITY = "multiplicity";


    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private int id;
    @DatabaseField(columnName = FIELD_MULTIPLICITY)
    private double multiplicity;

    public BarlowLensTableModel() {
    }

    public BarlowLensTableModel(BarlowLens barlowLens) {
        id = barlowLens.getId();
        multiplicity = barlowLens.getMultiplicity();
    }

    public int getId() {
        return id;
    }

    public double getMultiplicity() {
        return multiplicity;
    }
}
