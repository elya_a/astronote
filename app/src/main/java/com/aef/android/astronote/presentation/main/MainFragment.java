package com.aef.android.astronote.presentation.main;

import android.os.Bundle;

public class MainFragment extends BaseRecordListFragment {

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public boolean isPlanned() {
        return false;
    }
}
