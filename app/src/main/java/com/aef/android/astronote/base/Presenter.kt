package com.aef.android.astronote.base

interface Presenter<in V> {
    fun attach(view: V)
    fun detach()
}
