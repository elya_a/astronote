package com.aef.android.astronote.data.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class MatrixDao extends BaseDao<MatrixTableModel> {

    protected MatrixDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, MatrixTableModel.class);
    }
}
