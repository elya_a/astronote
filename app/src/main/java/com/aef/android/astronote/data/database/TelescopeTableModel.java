package com.aef.android.astronote.data.database;

import com.aef.android.astronote.data.model.Telescope;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import static com.aef.android.astronote.data.database.TelescopeTableModel.TELESCOPE_TABLE_NAME;

@DatabaseTable(tableName = TELESCOPE_TABLE_NAME)
public class TelescopeTableModel {

    public static final String TELESCOPE_TABLE_NAME = "telescopes";
    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_DIAMETER = "diameter";
    public static final String FIELD_FOCUS = "focus";
    public static final String FIELD_VIEW_FIELD = "viewField";
    public static final String FIELD_RESOLUTION = "angularResolution";
    public static final String FIELD_MAGNIFICATION = "magnification";
    public static final String FIELD_VIEW_FIELD_OCULAR = "viewFieldOcular";

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = FIELD_ID)
    private int id;
    @DatabaseField(columnName = FIELD_NAME)
    private String name;
    @DatabaseField(columnName = FIELD_DIAMETER)
    private int diameter;
    @DatabaseField(columnName = FIELD_FOCUS)
    private double focus;
    @DatabaseField(columnName = FIELD_VIEW_FIELD)
    private double viewField;
    @DatabaseField(columnName = FIELD_RESOLUTION)
    private double angularResolution;
    @DatabaseField(columnName = FIELD_MAGNIFICATION)
    private int magnification;

    public TelescopeTableModel() {
    }

    public TelescopeTableModel(Telescope telescope) {
        id = telescope.getId();
        name = telescope.getName();
        diameter = telescope.getDiameter();
        focus = telescope.getFocus();
        viewField = telescope.getViewField();
        angularResolution = telescope.getAngularResolution();
        magnification = telescope.getMagnitude();
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getDiameter() {
        return diameter;
    }

    public double getFocus() {
        return focus;
    }

    public double getViewField() {
        return viewField;
    }

    public double getAngularResolution() {
        return angularResolution;
    }

    public int getMagnification() {
        return magnification;
    }
}
