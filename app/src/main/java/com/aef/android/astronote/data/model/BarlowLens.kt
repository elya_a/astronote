package com.aef.android.astronote.data.model

import android.os.Parcel
import android.os.Parcelable

import com.aef.android.astronote.data.database.BarlowLensTableModel

import java.util.Locale

data class BarlowLens(
    var id: Int = 0,
    var multiplicity: Double = 0.0
) : Tool(ToolType.BARLOW_LENS), Parcelable {

    constructor(tableModel: BarlowLensTableModel) : this (
        tableModel.id,
        tableModel.multiplicity
    )

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readDouble()
    )

    override val title: String
        get() = String.format(Locale.getDefault(), "%.1fx", multiplicity)

    override fun equals(other: Any?) =
        (other as? BarlowLens)?.id == id

    override fun hashCode() = id

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(this.id)
        parcel.writeDouble(this.multiplicity)
    }

    override fun describeContents() = 0

    companion object {

        @JvmField
        @Suppress("unused")
        val CREATOR: Parcelable.Creator<BarlowLens> = object : Parcelable.Creator<BarlowLens> {
            override fun createFromParcel(source: Parcel) = BarlowLens(source)
            override fun newArray(size: Int) = arrayOfNulls<BarlowLens>(size)
        }
    }
}
