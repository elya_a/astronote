package com.aef.android.astronote.presentation.choose_tool;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.aef.android.astronote.R;
import com.aef.android.astronote.base.BaseThemedActivity;
import com.aef.android.astronote.data.model.Tool;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.aef.android.astronote.presentation.choose_tool.ChooseToolFragment.KEY_TOOL_LIST;
import static com.aef.android.astronote.presentation.choose_tool.ChooseToolFragment.KEY_TOOL_TYPE;

public class ChooseToolActivity<T extends Tool> extends BaseThemedActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public static <T extends Tool> void startForResult(Fragment fragment, int requestCode, @Tool.Type int toolType, ArrayList<T> selectedToolList) {
        Intent intent = new Intent(fragment.getContext(), ChooseToolActivity.class);
        intent.putExtra(KEY_TOOL_TYPE, toolType);
        intent.putExtra(KEY_TOOL_LIST, selectedToolList);
        fragment.startActivityForResult(intent, requestCode);
    }

    @SuppressWarnings("WrongConstant")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setHasBackButton();
        @Tool.Type int type = getIntent().getIntExtra(KEY_TOOL_TYPE, Tool.Companion.getCAMERA());
        showFragment(R.id.content, ChooseToolFragment.newInstance(getIntent().getExtras()), false);

        switch (type) {
            case Tool.Companion.getBARLOW_LENS():
                setTitle(R.string.barlow_lenses);
                break;
            case Tool.Companion.getCAMERA():
                setTitle(R.string.photo_cameras);
                break;
            case Tool.Companion.getFOCUS_GEAR():
                setTitle(R.string.focus_gears);
                break;
            case Tool.Companion.getMATRIX():
                setTitle(R.string.matrixes);
                break;
            case Tool.Companion.getOCULAR():
                setTitle(R.string.oculars);
                break;
            case Tool.Companion.getTELESCOPE():
                setTitle(R.string.telescopes);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        initDrawableColor(menu.findItem(R.id.action_done).getIcon(), android.R.color.white);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_done) {
            save();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment != null && fragment instanceof ChooseToolFragment) {
            ((ChooseToolFragment) fragment).done();
        }
    }
}
