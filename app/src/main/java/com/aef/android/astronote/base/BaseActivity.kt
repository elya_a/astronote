package com.aef.android.astronote.base


import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction

abstract class BaseActivity : AppCompatActivity() {

    protected fun showFragment(@IdRes containerId: Int, fragment: Fragment, addToBackStack: Boolean) {
        var startFragment: Fragment? = null
        // = getSupportFragmentManager().findFragmentByTag(fragment.getClass().getSimpleName());
        if (startFragment == null) {
            startFragment = fragment
        }
        val fragmentTransaction = supportFragmentManager.beginTransaction()
            .replace(containerId, startFragment, startFragment.javaClass.simpleName)
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null)
        }
        fragmentTransaction.commitAllowingStateLoss()
    }

    protected fun addFragment(@IdRes containerId: Int, fragment: Fragment, addToBackStack: Boolean) {
        var startFragment: Fragment? = null
        // = getSupportFragmentManager().findFragmentByTag(fragment.getClass().getSimpleName());
        if (startFragment == null) {
            startFragment = fragment
        }
        val fragmentTransaction = supportFragmentManager.beginTransaction()
            .add(containerId, startFragment, startFragment.javaClass.simpleName)
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null)
        }
        fragmentTransaction.commitAllowingStateLoss()
    }
}
