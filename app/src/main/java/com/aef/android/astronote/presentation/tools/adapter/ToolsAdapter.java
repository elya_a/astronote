package com.aef.android.astronote.presentation.tools.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.BarlowLens;
import com.aef.android.astronote.data.model.CCDMatrix;
import com.aef.android.astronote.data.model.FocusGear;
import com.aef.android.astronote.data.model.Ocular;
import com.aef.android.astronote.data.model.PhotoCamera;
import com.aef.android.astronote.data.model.Telescope;
import com.aef.android.astronote.data.model.Tool;
import com.aef.android.astronote.data.model.ToolType;

import java.util.ArrayList;

public class ToolsAdapter extends RecyclerView.Adapter<ToolViewHolder> {

    private ArrayList<Tool> toolArrayList;
    private ToolType toolType;
    private ToolItemCallback toolItemCallback;
    private boolean isDarkTheme;

    public ToolsAdapter(ToolType toolType, ToolItemCallback toolItemCallback, boolean isDarkTheme) {
        this.toolType = toolType;
        this.toolItemCallback = toolItemCallback;
        this.isDarkTheme = isDarkTheme;
    }

    public void setToolArrayList(ArrayList<T> toolArrayList) {
        this.toolArrayList = toolArrayList;
    }

    @Override
    public ToolViewHolder onCreateViewHolder(ViewGroup parent, ToolType viewType) {
        View view;
        ToolViewHolder toolViewHolder;
        switch (viewType) {
            case ToolType.BARLOW_LENS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_barlow_item, parent, false);
                toolViewHolder = new BarlowLensViewHolder(view, (ToolItemCallback<BarlowLens>) toolItemCallback);
                break;
            case Tool.Companion.getCAMERA():
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_camera_item, parent, false);
                toolViewHolder = new CameraViewHolder(view, (ToolItemCallback<PhotoCamera>) toolItemCallback);
                break;
            case Tool.Companion.getFOCUS_GEAR():
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_focus_item, parent, false);
                toolViewHolder = new FocusGearViewHolder(view, (ToolItemCallback<FocusGear>) toolItemCallback);
                break;
            case Tool.Companion.getMATRIX():
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_matrix_item, parent, false);
                toolViewHolder = new MatrixViewHolder(view, (ToolItemCallback<CCDMatrix>) toolItemCallback);
                break;
            case Tool.Companion.getOCULAR():
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_ocular_item, parent, false);
                toolViewHolder = new OcularViewHolder(view, (ToolItemCallback<Ocular>) toolItemCallback);
                break;
            case Tool.Companion.getTELESCOPE():
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_telescope_item, parent, false);
                toolViewHolder = new TelescopeViewHolder(view, (ToolItemCallback<Telescope>) toolItemCallback);
                break;
            default:
                return null;
        }
        toolViewHolder.setDark(isDarkTheme);
        return toolViewHolder;
    }

    @Override
    public void onBindViewHolder(ToolViewHolder holder, int position) {
        holder.setData(toolArrayList.get(position));
        /*switch (toolType) {
            case Tool.BARLOW_LENS:
                holder.setData(barlowLensArrayList.get(position));
                break;
            case Tool.CAMERA:
                holder.setData(cameraArrayList.get(position));
                break;
            case Tool.FOCUS_GEAR:
                holder.setData(focusGearArrayList.get(position));
                break;
            case Tool.MATRIX:
                holder.setData(matrixArrayList.get(position));
                break;
            case Tool.OCULAR:
                holder.setData(ocularArrayList.get(position));
                break;
            case Tool.TELESCOPE:
                holder.setData(telescopeArrayList.get(position));
                break;
        }*/
    }

    @Override
    public int getItemCount() {
        /*switch (toolType) {
            case Tool.CAMERA:
                return cameraArrayList.size();
            case Tool.MATRIX:
                return matrixArrayList.size();
            case Tool.TELESCOPE:
                return telescopeArrayList.size();
            case Tool.BARLOW_LENS:
                return barlowLensArrayList.size();
            case Tool.FOCUS_GEAR:
                return focusGearArrayList.size();
            case Tool.OCULAR:
                return ocularArrayList.size();
        }*/
        return toolArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return toolType;
    }
}
