package com.aef.android.astronote.presentation.choose_tool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aef.android.astronote.R;
import com.aef.android.astronote.base.BaseThemedFragment;
import com.aef.android.astronote.data.model.Tool;
import com.aef.android.astronote.presentation.choose_tool.adapter.ChooseToolAdapter;
import com.aef.android.astronote.presentation.tools.ToolListContract;
import com.aef.android.astronote.presentation.tools.ToolListPresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.recyclerview.widget.DividerItemDecoration.VERTICAL;


public class ChooseToolFragment<T extends Tool> extends BaseThemedFragment implements ToolListContract.View<T> {

    public static final String KEY_TOOL_TYPE = "KEY_TOOL_TYPE";
    public static final String KEY_TOOL_LIST = "KEY_TOOL_LIST";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Tool.Type
    private int toolType;
    private ToolListContract.Presenter presenter;
    private ChooseToolAdapter<T> adapter;
    private ArrayList<T> selectedList;

    public static <T extends Tool> ChooseToolFragment newInstance(@Tool.Type int toolType) {
        Bundle args = new Bundle();
        args.putInt(KEY_TOOL_TYPE, toolType);
        ChooseToolFragment<T> fragment = new ChooseToolFragment<>();
        fragment.setArguments(args);
        return fragment;
    }

    public static <T extends Tool> ChooseToolFragment newInstance(Bundle args) {
        ChooseToolFragment<T> fragment = new ChooseToolFragment<>();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("WrongConstant")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_tool, container, false);
        ButterKnife.bind(this, view);

        toolType = getArguments().getInt(KEY_TOOL_TYPE);
        selectedList = getArguments().getParcelableArrayList(KEY_TOOL_LIST);
        if (selectedList == null) {
            selectedList = new ArrayList<>();
        }

        setPresenter(new ToolListPresenter<T>(this, toolType));
        presenter.loadTools();

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), VERTICAL));

        return view;
    }

    void done() {
        Intent intent = new Intent();
        intent.putExtra(KEY_TOOL_LIST, adapter.getSelectedToolList());
        intent.putExtra(KEY_TOOL_TYPE, toolType);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public void setPresenter(ToolListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showToolList(ArrayList<T> toolArrayList) {
        adapter = new ChooseToolAdapter<>(toolArrayList, selectedList, isDarkTheme);
        recyclerView.setAdapter(adapter);
    }
}
