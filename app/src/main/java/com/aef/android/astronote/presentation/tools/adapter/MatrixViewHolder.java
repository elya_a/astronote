package com.aef.android.astronote.presentation.tools.adapter;

import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.aef.android.astronote.data.model.CCDMatrix;
import com.aef.android.astronote.databinding.LayoutMatrixItemBinding;

public class MatrixViewHolder extends ToolViewHolder<CCDMatrix> {

    private final LayoutMatrixItemBinding binding;

    public MatrixViewHolder(View itemView, ToolItemCallback<CCDMatrix> toolItemCallback) {
        super(itemView, toolItemCallback);
        binding = DataBindingUtil.bind(itemView);
    }

    public void setData(CCDMatrix matrix) {
        super.setData(matrix);
        binding.setMatrix(matrix);
    }
}
