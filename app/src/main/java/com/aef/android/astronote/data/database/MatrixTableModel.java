package com.aef.android.astronote.data.database;

import com.aef.android.astronote.data.model.CCDMatrix;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import static com.aef.android.astronote.data.database.MatrixTableModel.MATRIX_TABLE_NAME;

@DatabaseTable(tableName = MATRIX_TABLE_NAME)
public class MatrixTableModel {

    public static final String MATRIX_TABLE_NAME = "matrixes";
    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_PIXELS = "megaPixelCount";
    public static final String FIELD_SENSOR = "sensorSize";
    public static final String FIELD_CROP = "cropFactor";

    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    private int id;
    @DatabaseField(columnName = FIELD_NAME)
    private String name;
    @DatabaseField(columnName = FIELD_PIXELS)
    private double megaPixelCount;
    @DatabaseField(columnName = FIELD_SENSOR)
    private String sensorSize;
    @DatabaseField(columnName = FIELD_CROP)
    private double cropFactor;

    public MatrixTableModel() {
    }

    public MatrixTableModel(CCDMatrix matrix) {
        id = matrix.getId();
        name = matrix.getName();
        megaPixelCount = matrix.getMegaPixelCount();
        sensorSize = matrix.getSensorSize();
        cropFactor = matrix.getCropFactor();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getMegaPixelCount() {
        return megaPixelCount;
    }

    public String getSensorSize() {
        return sensorSize;
    }

    public double getCropFactor() {
        return cropFactor;
    }
}
