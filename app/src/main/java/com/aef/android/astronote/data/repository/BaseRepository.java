package com.aef.android.astronote.data.repository;

import com.aef.android.astronote.App;
import com.aef.android.astronote.data.database.DataBaseHelper;

public abstract class BaseRepository {

    protected DataBaseHelper dataBaseHelper;

    public BaseRepository() {
        dataBaseHelper = App.getDataBaseHelper();
    }
}
