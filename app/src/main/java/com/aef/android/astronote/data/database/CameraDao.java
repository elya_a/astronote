package com.aef.android.astronote.data.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class CameraDao extends BaseDao<CameraTableModel> {

    protected CameraDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, CameraTableModel.class);
    }
}
