package com.aef.android.astronote.data.database;

import com.aef.android.astronote.data.model.Tool;
import com.j256.ormlite.dao.BaseDaoImpl;

import java.sql.SQLException;

public class ToolDaoFabric {

    public static BaseDaoImpl getToolDao(@Tool.Type int toolType, DataBaseHelper dataBaseHelper) throws SQLException {
        switch (toolType) {
            case Tool.Companion.getBARLOW_LENS():
                return dataBaseHelper.getBarlowLensDao();
            case Tool.Companion.getCAMERA():
                return dataBaseHelper.getCameraDao();
            case Tool.Companion.getFOCUS_GEAR():
                return dataBaseHelper.getFocusGearDao();
            case Tool.Companion.getMATRIX():
                return dataBaseHelper.getMatrixDao();
            case Tool.Companion.getOCULAR():
                return dataBaseHelper.getOcularDao();
            case Tool.Companion.getTELESCOPE():
                return dataBaseHelper.getTelescopeDao();
            default:
                return null;
        }
    }
}
