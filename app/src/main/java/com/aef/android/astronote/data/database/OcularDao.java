package com.aef.android.astronote.data.database;

import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;


public class OcularDao extends BaseDao<OcularTableModel> {

    protected OcularDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, OcularTableModel.class);
    }
}
