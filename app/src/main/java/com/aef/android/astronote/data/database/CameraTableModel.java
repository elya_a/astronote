package com.aef.android.astronote.data.database;

import com.aef.android.astronote.data.model.PhotoCamera;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import static com.aef.android.astronote.data.database.CameraTableModel.CAMERA_TABLE_NAME;

@DatabaseTable(tableName = CAMERA_TABLE_NAME)
public class CameraTableModel {

    public static final String CAMERA_TABLE_NAME = "cameras";
    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_PIXELS = "megaPixelCount";
    public static final String FIELD_SENSOR = "sensorSize";
    public static final String FIELD_CROP = "cropFactor";

    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    private int id;
    @DatabaseField(columnName = FIELD_NAME)
    private String name;
    @DatabaseField(columnName = FIELD_PIXELS)
    private double megaPixelCount;
    @DatabaseField(columnName = FIELD_SENSOR)
    private String sensorSize;
    @DatabaseField(columnName = FIELD_CROP)
    private double cropFactor;

    public CameraTableModel() {
    }

    public CameraTableModel(PhotoCamera camera) {
        id = camera.getId();
        name = camera.getName();
        megaPixelCount = camera.getMegaPixelCount();
        sensorSize = camera.getSensorSize();
        cropFactor = camera.getCropFactor();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getMegaPixelCount() {
        return megaPixelCount;
    }

    public String getSensorSize() {
        return sensorSize;
    }

    public double getCropFactor() {
        return cropFactor;
    }
}
