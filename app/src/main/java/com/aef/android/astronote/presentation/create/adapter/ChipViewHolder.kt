package com.aef.android.astronote.presentation.create.adapter

import android.view.View

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView

import com.aef.android.astronote.databinding.LayoutChipItemBinding


class ChipViewHolder<T : ChipItem>(itemView: View, private val chipsListener: ChipsListener<T>?) :
    RecyclerView.ViewHolder(itemView) {

    private val binding: LayoutChipItemBinding? = DataBindingUtil.bind(itemView)
    private var chip: T? = null

    fun setData(chip: T) {
        this.chip = chip
        binding?.let {
            it.title = chip.title
            it.holder = this
            it.isDark = false
            it.closable = chipsListener != null
        }
    }

    fun onCloseClick() {
        chipsListener?.closeChip(chip!!)
    }
}
