package com.aef.android.astronote;

import android.app.Application;

import com.aef.android.astronote.data.database.DataBaseHelper;
import com.aef.android.astronote.utils.PreferenceHelper;

public class App extends Application {

    private static DataBaseHelper dataBaseHelper;
    private static PreferenceHelper preferenceHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        dataBaseHelper = new DataBaseHelper(this);
        preferenceHelper = new PreferenceHelper(this);
    }

    public static DataBaseHelper getDataBaseHelper() {
        return dataBaseHelper;
    }

    public static PreferenceHelper getPreferenceHelper() {
        return preferenceHelper;
    }
}
