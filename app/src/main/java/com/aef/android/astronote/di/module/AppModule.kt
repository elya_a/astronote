package com.aef.android.astronote.di.module

import android.content.Context
import com.aef.android.astronote.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val appContext: Context) {

    @Provides
    @Singleton
    fun provideAppContext(): Context =
        appContext
}