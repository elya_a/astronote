package com.aef.android.astronote.presentation.create;

import com.aef.android.astronote.data.model.AstroRecord;

import java.util.ArrayList;

class CreateContract {

    interface View extends com.aef.android.astronote.base.View<Presenter> {

        void recordSaved(AstroRecord astroRecord);

        void onError();

        void showPeople(ArrayList<String> people);
    }

    interface Presenter extends com.aef.android.astronote.base.Presenter {

        void saveAstroRecord(AstroRecord astroRecord);

        void loadPeople();
    }

}
