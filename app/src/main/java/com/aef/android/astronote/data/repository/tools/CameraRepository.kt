package com.aef.android.astronote.data.repository.tools

import com.aef.android.astronote.data.database.CameraTableModel
import com.aef.android.astronote.data.model.PhotoCamera
import com.aef.android.astronote.data.repository.BaseRepository
import com.google.gson.Gson

import java.sql.SQLException
import java.util.ArrayList

import io.reactivex.Observable
import io.reactivex.Single

class CameraRepository(private val gson: Gson) : BaseRepository(), BaseToolRepository<PhotoCamera> {

    override fun getToolList(): Observable<List<PhotoCamera>> =
        Observable.fromCallable {
            dataBaseHelper.cameraDao.queryForAll()
                .map { PhotoCamera(it) }
        }

    override fun getToolListByJsons(jsons: List<String>): Observable<List<PhotoCamera>> =
        Observable.fromCallable {
            val cameraDao = dataBaseHelper.cameraDao
            jsons.map { json ->
                val camera = gson.fromJson(json, PhotoCamera::class.java)
                if (cameraDao.getTableModel(camera.id) == null) {
                    camera.id = DEFAULT_ID
                }
                camera
            }
        }

    override fun saveTool(tool: PhotoCamera): Observable<PhotoCamera> =
        Observable.fromCallable {
            val tableModel = CameraTableModel(tool)
            dataBaseHelper.cameraDao.createOrUpdate(tableModel)
            PhotoCamera(tableModel)
        }

    override fun deleteTool(tool: PhotoCamera): Observable<PhotoCamera> =
        Observable.fromCallable {
            val tableModel = CameraTableModel(tool)
            dataBaseHelper.cameraDao.delete(tableModel)
            PhotoCamera(tableModel)
        }
}
