package com.aef.android.astronote.data.model

import android.os.Parcel
import android.os.Parcelable

import com.aef.android.astronote.data.database.TelescopeTableModel

data class Telescope(
    var id: Int = 0,
    var name: String? = null,
    var diameter: Int = 0,
    var focus: Double = 0.toDouble(),
    //поле зрения
    var viewField: Double = 0.toDouble(),
    //угловое разрешение
    var angularResolution: Double = 0.toDouble(),
    //проницающая сила/звездная величина
    var magnitude: Int = 0
) : Tool(ToolType.TELESCOPE), Parcelable {

    override val title: String
        get() = name ?: ""

    constructor(tableModel: TelescopeTableModel) : this(
        tableModel.id,
        tableModel.name,
        tableModel.diameter,
        tableModel.focus,
        tableModel.viewField,
        tableModel.angularResolution,
        tableModel.magnification
    )

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readInt()
    )

    override fun equals(other: Any?) =
        (other as? Telescope)?.id == id

    override fun hashCode() = id

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(this.id)
        dest.writeString(this.name)
        dest.writeInt(this.diameter)
        dest.writeDouble(this.focus)
        dest.writeDouble(this.viewField)
        dest.writeDouble(this.angularResolution)
        dest.writeInt(this.magnitude)
    }

    override fun describeContents() = 0

    companion object {

        @JvmField
        @Suppress("unused")
        val CREATOR: Parcelable.Creator<Telescope> = object : Parcelable.Creator<Telescope> {
            override fun createFromParcel(source: Parcel) = Telescope(source)
            override fun newArray(size: Int) = arrayOfNulls<Telescope>(size)
        }
    }
}
