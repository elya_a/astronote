package com.aef.android.astronote.data.repository.person

import com.aef.android.astronote.data.database.PersonTableModel
import com.aef.android.astronote.data.repository.BaseRepository

import java.sql.SQLException
import java.util.ArrayList

import io.reactivex.Observable

class PersonRepositoryImpl : BaseRepository(), PersonRepository {

    @Throws(SQLException::class)
    override fun savePersonList(list: ArrayList<String>?) {
        if (list != null) {
            val dao = dataBaseHelper.personDao
            for (name in list) {
                dao.createIfNotExists(PersonTableModel(name))
            }
        }
    }

    override fun getAllPersonsNames(): Observable<ArrayList<String>> =
        Observable.create { subscriber ->
            try {
                val dao = dataBaseHelper.personDao
                val names = ArrayList<String>()
                for (tableModel in dao.queryForAll()) {
                    names.add(tableModel.name)
                }
                subscriber.onNext(names)
                subscriber.onComplete()
            } catch (e: SQLException) {
                subscriber.onError(e)
            }
        }
}
