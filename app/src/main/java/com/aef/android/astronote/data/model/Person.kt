package com.aef.android.astronote.data.model

import android.os.Parcel
import android.os.Parcelable

import com.aef.android.astronote.data.database.PersonTableModel
import com.aef.android.astronote.presentation.create.adapter.ChipItem

data class Person(var name: String? = null) : ChipItem, Parcelable {

    override val title: String
        get() = name ?: ""

    constructor(tableModel: PersonTableModel) : this(tableModel.name)

    constructor(parcel: Parcel) : this(parcel.readString())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(this.name)
    }

    companion object {

        @JvmField
        @Suppress("unused")
        val CREATOR: Parcelable.Creator<Person> = object : Parcelable.Creator<Person> {
            override fun createFromParcel(source: Parcel) = Person(source)
            override fun newArray(size: Int) = arrayOfNulls<Person>(size)
        }
    }
}
