package com.aef.android.astronote.data.database;

import com.aef.android.astronote.data.model.Ocular;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import static com.aef.android.astronote.data.database.OcularTableModel.OCULAR_TABLE_NAME;
import static com.aef.android.astronote.data.database.TelescopeTableModel.FIELD_FOCUS;
import static com.aef.android.astronote.data.database.TelescopeTableModel.FIELD_VIEW_FIELD;

@DatabaseTable(tableName = OCULAR_TABLE_NAME)
public class OcularTableModel {

    public static final String OCULAR_TABLE_NAME = "oculars";

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private int id;
    @DatabaseField(columnName = FIELD_FOCUS)
    private double focus;
    @DatabaseField(columnName = FIELD_VIEW_FIELD)
    private double viewField;

    public OcularTableModel() {
    }

    public OcularTableModel(Ocular ocular) {
        id = ocular.getId();
        focus = ocular.getFocus();
        viewField = ocular.getViewField();
    }

    public int getId() {
        return id;
    }

    public double getFocus() {
        return focus;
    }

    public double getViewField() {
        return viewField;
    }
}
