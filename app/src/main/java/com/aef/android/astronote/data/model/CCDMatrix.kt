package com.aef.android.astronote.data.model

import android.os.Parcel
import android.os.Parcelable

import com.aef.android.astronote.data.database.MatrixTableModel
import com.google.gson.annotations.Expose

data class CCDMatrix(
    var id: Int = 0,
    var name: String? = null,
    var megaPixelCount: Double = 0.0,
    var sensorSize: String? = null,
    var cropFactor: Double = 0.0
) : Tool(ToolType.MATRIX), Parcelable {

    override val title: String
        get() = name ?: ""

    constructor(tableModel: MatrixTableModel) : this(
        tableModel.id,
        tableModel.name,
        tableModel.megaPixelCount,
        tableModel.sensorSize,
        tableModel.cropFactor
    )

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readDouble()
    )

    override fun equals(other: Any?) =
        (other as? CCDMatrix)?.id == id

    override fun hashCode() = id

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(this.id)
        dest.writeString(this.name)
        dest.writeDouble(this.megaPixelCount)
        dest.writeString(this.sensorSize)
        dest.writeDouble(this.cropFactor)
    }

    override fun describeContents() = 0

    companion object {

        @JvmField
        @Suppress("unused")
        val CREATOR: Parcelable.Creator<CCDMatrix> = object : Parcelable.Creator<CCDMatrix> {
            override fun createFromParcel(source: Parcel) = CCDMatrix(source)
            override fun newArray(size: Int) = arrayOfNulls<CCDMatrix>(size)
        }
    }
}
