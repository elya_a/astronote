package com.aef.android.astronote.data.repository.tools

import com.aef.android.astronote.data.database.MatrixTableModel
import com.aef.android.astronote.data.model.CCDMatrix
import com.aef.android.astronote.data.repository.BaseRepository
import com.google.gson.Gson

import java.sql.SQLException

import io.reactivex.Observable

class MatrixRepository(private val gson: Gson) : BaseRepository(), BaseToolRepository<CCDMatrix> {

    override fun getToolList(): Observable<List<CCDMatrix>> =
        Observable.fromCallable {
            dataBaseHelper.matrixDao.queryForAll()
                .map { CCDMatrix(it) }
        }

    override fun getToolListByJsons(jsons: List<String>): Observable<List<CCDMatrix>> =
        Observable.fromCallable {
            val matrixDao = dataBaseHelper.matrixDao
            jsons.map { json ->
                val matrix = gson.fromJson(json, CCDMatrix::class.java)
                if (matrixDao.getTableModel(matrix.id) == null) {
                    matrix.id = DEFAULT_ID
                }
                matrix
            }
        }

    override fun saveTool(tool: CCDMatrix): Observable<CCDMatrix> =
        Observable.fromCallable {
            val tableModel = MatrixTableModel(tool)
            dataBaseHelper.matrixDao.createOrUpdate(tableModel)
            CCDMatrix(tableModel)
        }

    override fun deleteTool(tool: CCDMatrix): Observable<CCDMatrix> =
        Observable.fromCallable {
            val tableModel = MatrixTableModel(tool)
            dataBaseHelper.matrixDao.delete(tableModel)
            CCDMatrix(tableModel)
        }
}
