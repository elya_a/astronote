package com.aef.android.astronote.data.repository.person

import io.reactivex.Observable
import java.util.ArrayList

interface PersonRepository {

    fun savePersonList(list: ArrayList<String>?)

    fun getAllPersonsNames(): Observable<ArrayList<String>>
}