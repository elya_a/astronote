package com.aef.android.astronote.data.model

import android.os.Parcel
import android.os.Parcelable

import com.aef.android.astronote.data.database.AstroRecordTableModel
import com.aef.android.astronote.data.database.BarlowLensTableModel
import com.aef.android.astronote.data.database.CameraTableModel
import com.aef.android.astronote.data.database.FocusGearTableModel
import com.aef.android.astronote.data.database.MatrixTableModel
import com.aef.android.astronote.data.database.OcularTableModel
import com.aef.android.astronote.data.database.TelescopeTableModel

import java.util.ArrayList

data class AstroRecord(
    var id: Int = 0,
    var weatherPressure: Int = 0,
    var weatherTemperature: String? = null,
    var weatherDescription: String? = null,
    var startDate: Long = 0,
    var startTime: Long = 0,
    var endDate: Long = 0,
    var endTime: Long = 0,
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var astroObjectList: List<AstroObject>? = null,
    var peopleList: List<Person>? = null,
    var telescopes: List<Telescope>? = null,
    var photoCameras: List<PhotoCamera>? = null,
    var ccdMatrixList: List<CCDMatrix>? = null,
    var oculars: List<Ocular>? = null,
    var barlowLensList: List<BarlowLens>? = null,
    //редуктор фокуса
    var focusGears: List<FocusGear>? = null
) : Parcelable {

    constructor(tableModel: AstroRecordTableModel) : this(
        tableModel.id,
        tableModel.weatherPressure,
        tableModel.weatherTemperature,
        tableModel.weatherDescription,
        tableModel.startDate,
        tableModel.startTime,
        tableModel.endDate,
        tableModel.endTime,
        tableModel.latitude,
        tableModel.longitude,
        null,
        tableModel.peopleList?.map { (Person(it)) },
        tableModel.telescopeTableModels?.map { Telescope(it) },
        tableModel.photoCameraTableModels?.map { PhotoCamera(it) },
        tableModel.ccdMatrixTableModels?.map { CCDMatrix(it) },
        tableModel.ocularsTableModels?.map { Ocular(it) },
        tableModel.barlowLensTableModels?.map { BarlowLens(it) },
        tableModel.focusGearTableModels?.map { FocusGear(it) }
    )

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readLong(),
        parcel.readLong(),
        parcel.readLong(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.createTypedArrayList(AstroObject.CREATOR),
        parcel.createTypedArrayList(Person.CREATOR),
        parcel.createTypedArrayList(Telescope.CREATOR),
        parcel.createTypedArrayList(PhotoCamera.CREATOR),
        parcel.createTypedArrayList(CCDMatrix.CREATOR),
        parcel.createTypedArrayList(Ocular.CREATOR),
        parcel.createTypedArrayList(BarlowLens.CREATOR),
        parcel.createTypedArrayList(FocusGear.CREATOR)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(this.id)
        parcel.writeInt(this.weatherPressure)
        parcel.writeString(this.weatherTemperature)
        parcel.writeString(this.weatherDescription)
        parcel.writeLong(this.startDate)
        parcel.writeLong(this.startTime)
        parcel.writeLong(this.endDate)
        parcel.writeLong(this.endTime)
        parcel.writeDouble(this.latitude)
        parcel.writeDouble(this.longitude)
        parcel.writeTypedList(this.astroObjectList)
        parcel.writeTypedList(this.peopleList)
        parcel.writeTypedList(this.telescopes)
        parcel.writeTypedList(this.photoCameras)
        parcel.writeTypedList(this.ccdMatrixList)
        parcel.writeTypedList(this.oculars)
        parcel.writeTypedList(this.barlowLensList)
        parcel.writeTypedList(this.focusGears)
    }

    override fun describeContents() = 0

    companion object {

        @JvmField
        @Suppress("unused")
        val CREATOR: Parcelable.Creator<AstroRecord> = object : Parcelable.Creator<AstroRecord> {
            override fun createFromParcel(source: Parcel) = AstroRecord(source)
            override fun newArray(size: Int) = arrayOfNulls<AstroRecord>(size)
        }
    }
}
