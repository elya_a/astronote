package com.aef.android.astronote.presentation.main;

import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.data.repository.astrorecord.AstroRecordRepositoryImpl;

import java.util.ArrayList;
import java.util.Calendar;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;
    private AstroRecordRepositoryImpl astroRecordRepository;

    public MainPresenter(MainContract.View view) {
        this.view = view;
        astroRecordRepository = new AstroRecordRepositoryImpl();
    }

    @Override
    public void loadAstroRecords(boolean isPlanned) {
        ArrayList<AstroRecord> astroRecords = new ArrayList<>();
        astroRecordRepository.loadAllAstroRecords()
                .flatMap(Observable::fromIterable)
                .filter(astroRecord -> {
                    if (astroRecord.getStartDate() == 0 && astroRecord.getEndDate() == 0) {
                        return isPlanned;
                    }

                    Calendar calendar = Calendar.getInstance();
                    Calendar targetCalendar = Calendar.getInstance();
                    targetCalendar.setTimeInMillis(astroRecord.getStartDate() != 0 ?
                            astroRecord.getStartDate() : astroRecord.getEndDate());
                    calendar.set(Calendar.HOUR_OF_DAY, 0);
                    calendar.set(Calendar.MINUTE, 0);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);
                    targetCalendar.set(Calendar.HOUR_OF_DAY, 0);
                    targetCalendar.set(Calendar.MINUTE, 0);
                    targetCalendar.set(Calendar.SECOND, 0);
                    targetCalendar.set(Calendar.MILLISECOND, 0);
                    return !((calendar.compareTo(targetCalendar) <= 0) ^ isPlanned);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(astroRecords::add, throwable -> {
                    throwable.printStackTrace();
                    view.onError();
                }, () -> view.showAstroRecords(astroRecords));
    }
}
