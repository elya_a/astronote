package com.aef.android.astronote.presentation.tools.adapter;

import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.aef.android.astronote.data.model.PhotoCamera;
import com.aef.android.astronote.databinding.LayoutCameraItemBinding;

public class CameraViewHolder extends ToolViewHolder<PhotoCamera> {

    private final LayoutCameraItemBinding binding;

    public CameraViewHolder(View itemView, ToolItemCallback<PhotoCamera> toolItemCallback) {
        super(itemView, toolItemCallback);
        binding = DataBindingUtil.bind(itemView);
    }

    public void setData(PhotoCamera camera) {
        super.setData(camera);
        binding.setCamera(camera);
    }
}
