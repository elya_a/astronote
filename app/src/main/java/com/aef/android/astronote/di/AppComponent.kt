package com.aef.android.astronote.di

import com.aef.android.astronote.App
import com.aef.android.astronote.di.module.AppModule
import dagger.Component

@Component(modules = arrayOf(
    AppModule::class
))
interface AppComponent {

    fun inject(app: App)
}