package com.aef.android.astronote.data.repository.tools

import com.aef.android.astronote.data.model.Tool
import com.aef.android.astronote.data.repository.BaseRepository

import java.util.ArrayList

import io.reactivex.Observable

const val DEFAULT_ID = -1

interface BaseToolRepository<T : Tool> {

    fun getToolList(): Observable<List<T>>

    fun getToolListByJsons(jsons: List<String>): Observable<List<T>>

    fun saveTool(tool: T): Observable<T>

    fun deleteTool(tool: T): Observable<T>
}
