package com.aef.android.astronote.base

import android.content.Context

abstract class BaseInteractionFragment<T : BaseFragmentInteraction> : BaseThemedFragment() {

    protected lateinit var fragmentInteractionCallback: T

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            fragmentInteractionCallback = context as T
        } catch (e: ClassCastException) {
            throw ClassCastException(
                context!!.toString()
                    + " must implement " + fragmentInteractionCallback.toString()
            )
        }

    }
}
