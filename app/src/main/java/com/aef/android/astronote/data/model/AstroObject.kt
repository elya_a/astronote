package com.aef.android.astronote.data.model

import android.os.Parcel
import android.os.Parcelable

import com.aef.android.astronote.presentation.create.adapter.ChipItem

data class AstroObject(
    val type: Type,
    val name: String,
    val coordinates: String,
    val magnitude: Double = 0.toDouble(),
    val data: String? = null,
    val comment: String? = null
) : Parcelable, ChipItem {

    override val title: String
        get() = name

    constructor(parcel: Parcel) : this(
        parcel.readSerializable() as Type,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readDouble(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeSerializable(type)
        parcel.writeString(name)
        parcel.writeString(coordinates)
        parcel.writeDouble(magnitude)
        parcel.writeString(data)
        parcel.writeString(comment)
    }

    override fun describeContents() = 0

    companion object {

        @JvmField
        @Suppress("unused")
        val CREATOR: Parcelable.Creator<AstroObject> = object : Parcelable.Creator<AstroObject> {
            override fun createFromParcel(source: Parcel) = AstroObject(source)
            override fun newArray(size: Int) = arrayOfNulls<AstroObject>(size)
        }
    }

    enum class Type
}
