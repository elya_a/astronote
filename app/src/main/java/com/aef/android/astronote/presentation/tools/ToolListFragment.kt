package com.aef.android.astronote.presentation.tools

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.aef.android.astronote.R
import com.aef.android.astronote.base.BaseInteractionFragment
import com.aef.android.astronote.data.model.Tool
import com.aef.android.astronote.presentation.main.MainActivityFragmentInteraction
import com.aef.android.astronote.presentation.tools.adapter.ToolItemCallback
import com.aef.android.astronote.presentation.tools.adapter.ToolsAdapter
import com.aef.android.astronote.utils.DialogHelper

import java.util.ArrayList

import butterknife.BindView
import butterknife.ButterKnife
import com.aef.android.astronote.data.model.ToolType

class ToolListFragment :
    BaseInteractionFragment<MainActivityFragmentInteraction>(),
    ToolListContract.ToolListView,
    ToolItemCallback {

    @BindView(R.id.recycler_view)
    internal var recyclerView: RecyclerView? = null

    lateinit var presenter: ToolListContract.ToolListPresenter

    private lateinit var toolsAdapter: ToolsAdapter
    private lateinit var toolType: ToolType
    private var tools: MutableList<Tool>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_tool_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this, view)

        toolType = arguments?.getSerializable(KEY_TOOL_TYPE) as ToolType

        toolsAdapter = ToolsAdapter(toolType, this, isDarkTheme)
        initRecyclerView()

        presenter.attach(this)
        presenter.loadTools()

    }

    override fun showToolList(toolArrayList: List<Tool>) {
        tools = toolArrayList.toMutableList()
        toolsAdapter.setToolArrayList(tools)
        initRecyclerView()
    }


    private fun initRecyclerView() {
        recyclerView!!.layoutManager = LinearLayoutManager(activity)
        recyclerView!!.adapter = toolsAdapter
    }

    fun updateTool(tool: Tool) {
        if (tools == null) {
            tools = ArrayList()
        }
        tools?.let {
            if (it.contains(tool)) {
                val index = it.indexOf(tool)
                it[index] = tool
                toolsAdapter.notifyItemChanged(index)
            } else {
                it.add(tool)
                toolsAdapter.notifyItemInserted(it.size - 1)
            }
        }
    }

    override fun onEditItem(tool: Tool) {
        fragmentInteractionCallback.showEditToolScreen(tool)
    }

    override fun onDeleteItem(tool: Tool, position: Int) {
        DialogHelper.showConfirmDialog(
            context, null, getString(R.string.delete_question),
            { dialog, which ->
                presenter.deleteTool(tool)
                tools!!.removeAt(position)
                toolsAdapter!!.notifyItemRemoved(position)
            }, null
        )
    }

    companion object {

        val KEY_TOOL_TYPE = "KEY_TOOL_TYPE"

        fun newInstance(toolType: ToolType): ToolListFragment {
            val args = Bundle()
            args.putSerializable(KEY_TOOL_TYPE, toolType)
            val fragment = ToolListFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
