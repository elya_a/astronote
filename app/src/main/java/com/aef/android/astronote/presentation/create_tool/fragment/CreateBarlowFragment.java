package com.aef.android.astronote.presentation.create_tool.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.BarlowLens;

import butterknife.BindView;

public class CreateBarlowFragment extends BaseCreateToolFragment<BarlowLens> {

    @BindView(R.id.multi_et)
    EditText multiplicityEditText;

    public static CreateBarlowFragment newInstance(BarlowLens barlowLens) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_TOOL, barlowLens);
        CreateBarlowFragment fragment = new CreateBarlowFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_create_barlow;
    }

    @Override
    protected BarlowLens newTool() {
        return new BarlowLens();
    }

    @Override
    protected void initFields(BarlowLens tool) {
        multiplicityEditText.setText(String.valueOf(tool.getMultiplicity()));
    }

    @Override
    protected boolean isValid() {
        return !TextUtils.isEmpty(multiplicityEditText.getText().toString());
    }

    @Override
    protected BarlowLens getToolModel() {
        tool.setMultiplicity(Double.valueOf(multiplicityEditText.getText().toString()));
        return tool;
    }
}
