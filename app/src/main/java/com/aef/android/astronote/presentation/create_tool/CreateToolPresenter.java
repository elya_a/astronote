package com.aef.android.astronote.presentation.create_tool;

import com.aef.android.astronote.data.model.BarlowLens;
import com.aef.android.astronote.data.model.CCDMatrix;
import com.aef.android.astronote.data.model.FocusGear;
import com.aef.android.astronote.data.model.Ocular;
import com.aef.android.astronote.data.model.PhotoCamera;
import com.aef.android.astronote.data.model.Telescope;
import com.aef.android.astronote.data.repository.tools.ToolsRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CreateToolPresenter implements CreateToolContract.Presenter {

    private CreateToolContract.View view;
    private ToolsRepository toolsRepository;

    public CreateToolPresenter(CreateToolContract.View view) {
        this.view = view;
        toolsRepository = new ToolsRepository();
    }

    @Override
    public void saveCamera(PhotoCamera camera) {
        toolsRepository.saveCamera(camera)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::toolSaved, Throwable::printStackTrace);
    }

    @Override
    public void saveTelescope(Telescope telescope) {
        toolsRepository.saveTelescope(telescope)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::toolSaved, Throwable::printStackTrace);
    }

    @Override
    public void saveMatrix(CCDMatrix matrix) {
        toolsRepository.saveMatrix(matrix)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::toolSaved, Throwable::printStackTrace);
    }

    @Override
    public void saveOcular(Ocular ocular) {
        toolsRepository.saveOcular(ocular)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::toolSaved, Throwable::printStackTrace);
    }

    @Override
    public void saveBarlowLens(BarlowLens barlowLens) {
        toolsRepository.saveBarlowLens(barlowLens)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::toolSaved, Throwable::printStackTrace);
    }

    @Override
    public void saveFocusGear(FocusGear focusGear) {
        toolsRepository.saveFocusGear(focusGear)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::toolSaved, Throwable::printStackTrace);
    }
}
