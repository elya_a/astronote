package com.aef.android.astronote.presentation.create_tool.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.Telescope;

import butterknife.BindView;

public class CreateTelescopeFragment extends BaseCreateToolFragment<Telescope> {

    @BindView(R.id.name_et)
    EditText nameEditText;
    @BindView(R.id.diameter_et)
    EditText diameterEditText;
    @BindView(R.id.focus_et)
    EditText focusEditText;
    @BindView(R.id.view_field_et)
    EditText viewFieldEditText;
    @BindView(R.id.resolution_et)
    EditText resolutionEditText;
    @BindView(R.id.magnitude_et)
    EditText magnitudeEditText;

    public static BaseCreateToolFragment newInstance(Telescope tool) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_TOOL, tool);
        CreateTelescopeFragment fragment = new CreateTelescopeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_create_telescope;
    }

    @Override
    protected Telescope newTool() {
        return new Telescope();
    }

    @Override
    protected void initFields(Telescope tool) {
        nameEditText.setText(tool.getName());
        diameterEditText.setText(String.valueOf(tool.getDiameter()));
        focusEditText.setText(String.valueOf(tool.getFocus()));
        viewFieldEditText.setText(String.valueOf(tool.getViewField()));
        resolutionEditText.setText(String.valueOf(tool.getAngularResolution()));
        magnitudeEditText.setText(String.valueOf(tool.getMagnitude()));
    }

    @Override
    protected boolean isValid() {
        return  !(TextUtils.isEmpty(nameEditText.getText()) || TextUtils.isEmpty(diameterEditText.getText()) ||
        TextUtils.isEmpty(focusEditText.getText()));
    }

    @Override
    public Telescope getToolModel() {
        Telescope telescope = tool;
        telescope.setName(nameEditText.getText().toString());
        telescope.setDiameter(Integer.valueOf(diameterEditText.getText().toString()));
        telescope.setFocus(Double.valueOf(focusEditText.getText().toString()));
        if (!TextUtils.isEmpty(viewFieldEditText.getText().toString())) {
            telescope.setViewField(Double.valueOf(viewFieldEditText.getText().toString()));
        }
        if (!TextUtils.isEmpty(resolutionEditText.getText().toString())) {
            telescope.setAngularResolution(Double.valueOf(resolutionEditText.getText().toString()));
        }
        if (!TextUtils.isEmpty(magnitudeEditText.getText().toString())) {
            telescope.setMagnitude(Integer.valueOf(magnitudeEditText.getText().toString()));
        }
        return telescope;
    }
}
