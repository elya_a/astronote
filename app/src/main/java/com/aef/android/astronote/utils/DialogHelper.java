package com.aef.android.astronote.utils;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.aef.android.astronote.R;

import java.util.Calendar;

public class DialogHelper {

    public static void showDatePickerDialog(Context context, long millis,
                                            DatePickerDialog.OnDateSetListener onDateSetListener,
                                            @Nullable Long minDate, @Nullable Long maxDate) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(millis);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, onDateSetListener, year, month, day);
        if (minDate != null) {
            datePickerDialog.getDatePicker().setMinDate(minDate);
        }
        if (maxDate != null) {
            datePickerDialog.getDatePicker().setMaxDate(maxDate);
        }
        datePickerDialog.show();
    }

    public static void showTimePickerDialog(Context context, int hourOfDay, int minute,
                                     TimePickerDialog.OnTimeSetListener onTimeSetListener) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, onTimeSetListener,
                hourOfDay, minute, true);
        timePickerDialog.show();
    }

    public static void showConfirmDialog(Context context, String title, String text,
                                         DialogInterface.OnClickListener onPositiveClickListener,
                                         DialogInterface.OnClickListener onNegativeClickListener) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(R.string.yes, onPositiveClickListener)
                .setNegativeButton(R.string.cancel, onNegativeClickListener)
                .create()
                .show();
    }
}
