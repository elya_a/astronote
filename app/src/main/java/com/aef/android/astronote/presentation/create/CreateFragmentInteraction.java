package com.aef.android.astronote.presentation.create;

import com.aef.android.astronote.base.BaseFragmentInteraction;
import com.aef.android.astronote.data.model.AstroRecord;

public interface CreateFragmentInteraction extends BaseFragmentInteraction {

    void astroRecordSaved(AstroRecord astroRecord);

    void showMap(double latitude, double longitude);
}
