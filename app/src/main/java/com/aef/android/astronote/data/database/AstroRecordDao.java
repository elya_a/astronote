package com.aef.android.astronote.data.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

public class AstroRecordDao extends BaseDaoImpl<AstroRecordTableModel, Integer> {

    protected AstroRecordDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, AstroRecordTableModel.class);
    }

    public List<AstroRecordTableModel> getAllRecords() throws SQLException {
        return queryForAll();
    }
}
