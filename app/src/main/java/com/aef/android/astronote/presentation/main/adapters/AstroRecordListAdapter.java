package com.aef.android.astronote.presentation.main.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.utils.DateFormatHelper;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AstroRecordListAdapter extends RecyclerView.Adapter<AstroRecordListAdapter.AstroRecordViewHolder> {

    private ArrayList<AstroRecord> astroRecords;
    private AstroCallback astroCallback;

    public AstroRecordListAdapter(ArrayList<AstroRecord> astroRecords, AstroCallback astroCallback) {
        this.astroRecords = astroRecords;
        this.astroCallback = astroCallback;
    }

    @Override
    public AstroRecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_astrorecord_item, parent, false);
        return new AstroRecordViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AstroRecordViewHolder holder, int position) {
        holder.bind(astroRecords.get(position));
    }

    @Override
    public int getItemCount() {
        return astroRecords.size();
    }

    public interface AstroCallback {

        void showAstroRecord(AstroRecord astroRecord);

        void editAstroRecord(AstroRecord astroRecord);

        void deleteAstroRecord(AstroRecord astroRecord);
    }

    class AstroRecordViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.start_date_tv)
        TextView startDateTextView;
        @BindView(R.id.end_date_tv)
        TextView endDateTextView;

        public AstroRecordViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (astroCallback != null) {
                itemView.setOnClickListener(view -> {
                    astroCallback.showAstroRecord(astroRecords.get(getAdapterPosition()));
                });
                itemView.setOnLongClickListener(view -> {
                    String[] list = {itemView.getContext().getString(R.string.edit),
                            itemView.getContext().getString(R.string.delete)};
                    new AlertDialog.Builder(itemView.getContext())
                            .setItems(list, (dialogInterface, i) -> {
                                switch (i) {
                                    case 0:
                                        astroCallback.editAstroRecord(astroRecords.get(getAdapterPosition()));
                                        break;
                                    case 1:
                                        astroCallback.deleteAstroRecord(astroRecords.get(getAdapterPosition()));
                                        break;
                                }
                            })
                            .create()
                            .show();
                    return true;
                });
            }
        }

        void bind(AstroRecord astroRecord) {
            startDateTextView.setText(String.format(Locale.getDefault(), "%s %s",
                    DateFormatHelper.getDate(astroRecord.getStartDate()),
                    DateFormatHelper.getTime(astroRecord.getStartTime())));
            endDateTextView.setText(String.format(Locale.getDefault(), "%s %s",
                    DateFormatHelper.getDate(astroRecord.getEndDate()),
                    DateFormatHelper.getTime(astroRecord.getEndTime())));
        }
    }
}
