package com.aef.android.astronote.presentation.tools.adapter;


import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.aef.android.astronote.data.model.BarlowLens;
import com.aef.android.astronote.databinding.LayoutBarlowItemBinding;

public class BarlowLensViewHolder extends ToolViewHolder<BarlowLens> {

    private LayoutBarlowItemBinding binding;

    public BarlowLensViewHolder(View itemView, ToolItemCallback<BarlowLens> toolItemCallback) {
        super(itemView, toolItemCallback);
        binding = DataBindingUtil.bind(itemView);
    }

    @Override
    public void setData(BarlowLens data) {
        super.setData(data);
        binding.setBarlow(data);
    }
}
