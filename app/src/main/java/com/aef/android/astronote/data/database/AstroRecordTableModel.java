package com.aef.android.astronote.data.database;

import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.data.model.BarlowLens;
import com.aef.android.astronote.data.model.CCDMatrix;
import com.aef.android.astronote.data.model.FocusGear;
import com.aef.android.astronote.data.model.Ocular;
import com.aef.android.astronote.data.model.Person;
import com.aef.android.astronote.data.model.PhotoCamera;
import com.aef.android.astronote.data.model.Telescope;
import com.google.gson.Gson;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;

import static com.aef.android.astronote.data.database.AstroRecordTableModel.TABLE_NAME;

@DatabaseTable(tableName = TABLE_NAME)
public class AstroRecordTableModel {
    public static final String TABLE_NAME = "astroRecord";
    public static final String FIELD_LATITUDE = "latitude";
    public static final String FIELD_LAONGITUDE = "longitude";
    public static final String FIELD_PRESSURE = "weatherPressure";
    public static final String FIELD_TEMPERATURE = "weatherTemperature";
    public static final String FIELD_WEATHER = "weatherDescription";
    public static final String FIELD_START_DATE = "startDate";
    public static final String FIELD_START_TIME = "startTime";
    public static final String FIELD_END_DATE = "endDate";
    public static final String FIELD_END_TIME = "endTime";
    public static final String FIELD_PEOPLE = "peopleList";
    public static final String FIELD_TELESCOPES = "telescopes";
    public static final String FIELD_CAMERAS = "photoCameras";
    public static final String FIELD_MATRIX = "ccdMatrixList";
    public static final String FIELD_OBJECTS = "astroObjectList";
    public static final String FIELD_OCULARS = "ocularIdList";
    public static final String FIELD_BARLOW_LENS = "barlowLensList";
    public static final String FIELD_FOCUS_GEARS = "focusGearList";

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private int id;
    @DatabaseField(columnName = FIELD_LATITUDE)
    private double latitude;
    @DatabaseField(columnName = FIELD_LAONGITUDE)
    private double longitude;
    @DatabaseField(columnName = FIELD_PRESSURE)
    private int weatherPressure;
    @DatabaseField(columnName = FIELD_TEMPERATURE)
    private String weatherTemperature;
    @DatabaseField(columnName = FIELD_WEATHER)
    private String weatherDescription;
    @DatabaseField(columnName = FIELD_START_DATE)
    private long startDate;
    @DatabaseField(columnName = FIELD_START_TIME)
    private long startTime;
    @DatabaseField(columnName = FIELD_END_DATE)
    private long endDate;
    @DatabaseField(columnName = FIELD_END_TIME)
    private long endTime;
    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = FIELD_PEOPLE)
    private ArrayList<String> peopleList;
    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = FIELD_TELESCOPES)
    private ArrayList<String> telescopes;
    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = FIELD_CAMERAS)
    private ArrayList<String> photoCameras;
    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = FIELD_MATRIX)
    private ArrayList<String> ccdMatrixList;
    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = FIELD_OCULARS)
    private ArrayList<String> ocularIdList;
    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = FIELD_BARLOW_LENS)
    private ArrayList<String> barlowLensIdList;
    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = FIELD_FOCUS_GEARS)
    private ArrayList<String> focusGearIdList;
    //@DatabaseField(dataType = DataType.SERIALIZABLE, columnName = FIELD_OBJECTS)
    //private ArrayList<AstroObject> astroObjectList;
    private ArrayList<TelescopeTableModel> telescopeTableModels;
    private ArrayList<CameraTableModel> photoCameraTableModels;
    private ArrayList<MatrixTableModel> ccdMatrixTableModels;
    private ArrayList<OcularTableModel> ocularsTableModels;
    private ArrayList<BarlowLensTableModel> barlowLensTableModels;
    private ArrayList<FocusGearTableModel> focusGearTableModels;

    public AstroRecordTableModel() {
    }

    public AstroRecordTableModel(AstroRecord astroRecord) {
        id = astroRecord.getId();
        latitude = astroRecord.getLatitude();
        longitude = astroRecord.getLongitude();
        weatherDescription = astroRecord.getWeatherDescription();
        weatherPressure = astroRecord.getWeatherPressure();
        weatherTemperature = astroRecord.getWeatherTemperature();
        startDate = astroRecord.getStartDate();
        startTime = astroRecord.getStartTime();
        endDate = astroRecord.getEndDate();
        endTime = astroRecord.getEndTime();
        if (astroRecord.getPeopleList() != null) {
            peopleList = new ArrayList<>();
            for (Person person: astroRecord.getPeopleList()) {
                peopleList.add(person.getName());
            }
        }
        Gson gson = new Gson();
        if (astroRecord.getTelescopes() != null) {
            telescopes = new ArrayList<>();
            telescopeTableModels = new ArrayList<>();
            for (Telescope telescope : astroRecord.getTelescopes()) {
                telescopes.add(gson.toJson(telescope));
                telescopeTableModels.add(new TelescopeTableModel(telescope));
            }
        }
        if (astroRecord.getPhotoCameras() != null) {
            photoCameras = new ArrayList<>();
            photoCameraTableModels = new ArrayList<>();
            for (PhotoCamera camera : astroRecord.getPhotoCameras()) {
                photoCameras.add(gson.toJson(camera));
                photoCameraTableModels.add(new CameraTableModel(camera));
            }
        }
        if (astroRecord.getCcdMatrixList() != null) {
            ccdMatrixList = new ArrayList<>();
            ccdMatrixTableModels = new ArrayList<>();
            for (CCDMatrix matrix : astroRecord.getCcdMatrixList()) {
                ccdMatrixList.add(gson.toJson(matrix));
                ccdMatrixTableModels.add(new MatrixTableModel(matrix));
            }
        }
        if (astroRecord.getOculars() != null) {
            ocularIdList = new ArrayList<>();
            ocularsTableModels = new ArrayList<>();
            for (Ocular ocular : astroRecord.getOculars()) {
                ocularIdList.add(gson.toJson(ocular));
                ocularsTableModels.add(new OcularTableModel(ocular));
            }
        }
        if (astroRecord.getBarlowLensList() != null) {
            barlowLensIdList = new ArrayList<>();
            barlowLensTableModels = new ArrayList<>();
            for (BarlowLens barlowLens : astroRecord.getBarlowLensList()) {
                barlowLensIdList.add(gson.toJson(barlowLens));
                barlowLensTableModels.add(new BarlowLensTableModel(barlowLens));
            }
        }
        if (astroRecord.getFocusGears() != null) {
            focusGearIdList = new ArrayList<>();
            focusGearTableModels = new ArrayList<>();
            for (FocusGear focusGear : astroRecord.getFocusGears()) {
                focusGearIdList.add(gson.toJson(focusGear));
                focusGearTableModels.add(new FocusGearTableModel());
            }
        }
    }

    public int getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getWeatherPressure() {
        return weatherPressure;
    }

    public String getWeatherTemperature() {
        return weatherTemperature;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public long getStartDate() {
        return startDate;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndDate() {
        return endDate;
    }

    public long getEndTime() {
        return endTime;
    }

    public ArrayList<String> getPeopleList() {
        return peopleList;
    }

    public ArrayList<String> getTelescopes() {
        return telescopes;
    }

    public ArrayList<String> getPhotoCameras() {
        return photoCameras;
    }

    public ArrayList<String> getCcdMatrixList() {
        return ccdMatrixList;
    }

    public ArrayList<TelescopeTableModel> getTelescopeTableModels() {
        return telescopeTableModels;
    }

    public void setTelescopeTableModels(ArrayList<TelescopeTableModel> telescopeTableModels) {
        this.telescopeTableModels = telescopeTableModels;
    }

    public ArrayList<CameraTableModel> getPhotoCameraTableModels() {
        return photoCameraTableModels;
    }

    public void setPhotoCameraTableModels(ArrayList<CameraTableModel> photoCameraTableModels) {
        this.photoCameraTableModels = photoCameraTableModels;
    }

    public ArrayList<MatrixTableModel> getCcdMatrixTableModels() {
        return ccdMatrixTableModels;
    }

    public void setCcdMatrixTableModels(ArrayList<MatrixTableModel> ccdMatrixTableModels) {
        this.ccdMatrixTableModels = ccdMatrixTableModels;
    }

    public ArrayList<String> getOcularIdList() {
        return ocularIdList;
    }

    public void setOcularIdList(ArrayList<String> ocularIdList) {
        this.ocularIdList = ocularIdList;
    }

    public ArrayList<String> getBarlowLensIdList() {
        return barlowLensIdList;
    }

    public void setBarlowLensIdList(ArrayList<String> barlowLensIdList) {
        this.barlowLensIdList = barlowLensIdList;
    }

    public ArrayList<String> getFocusGearIdList() {
        return focusGearIdList;
    }

    public void setFocusGearIdList(ArrayList<String> focusGearIdList) {
        this.focusGearIdList = focusGearIdList;
    }

    public ArrayList<OcularTableModel> getOcularsTableModels() {
        return ocularsTableModels;
    }

    public void setOcularsTableModels(ArrayList<OcularTableModel> ocularsTableModels) {
        this.ocularsTableModels = ocularsTableModels;
    }

    public ArrayList<BarlowLensTableModel> getBarlowLensTableModels() {
        return barlowLensTableModels;
    }

    public void setBarlowLensTableModels(ArrayList<BarlowLensTableModel> barlowLensTableModels) {
        this.barlowLensTableModels = barlowLensTableModels;
    }

    public ArrayList<FocusGearTableModel> getFocusGearTableModels() {
        return focusGearTableModels;
    }

    public void setFocusGearTableModels(ArrayList<FocusGearTableModel> focusGearTableModels) {
        this.focusGearTableModels = focusGearTableModels;
    }
}
