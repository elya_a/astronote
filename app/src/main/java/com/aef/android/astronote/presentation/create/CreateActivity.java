package com.aef.android.astronote.presentation.create;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.aef.android.astronote.R;
import com.aef.android.astronote.base.BaseThemedActivity;
import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.utils.DialogHelper;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateActivity extends BaseThemedActivity implements CreateFragmentInteraction {

    public static final double DEFAULT_LATITUDE = 56.84983;
    public static final double DEFAULT_LONGITUDE = 53.21838;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private LatLng selectedLatLng;

    public static void startForResult(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, CreateActivity.class);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void startForResult(Activity activity, int requestCode, @NonNull AstroRecord astroRecord) {
        Intent intent = new Intent(activity, CreateActivity.class);
        intent.putExtra(CreateFragment.KEY_ASTRO_RECORD, astroRecord);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setHasBackButton();
        setTitle(R.string.create);
        showFragment(R.id.content, CreateFragment.newInstance(getIntent().getExtras()), false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        initDrawableColor(menu.findItem(R.id.action_done).getIcon(), android.R.color.white);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_done) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
            if (fragment instanceof CreateFragment) {
                save();
            } else if (fragment instanceof SupportMapFragment) {
                getSupportFragmentManager().popBackStack();
                ((CreateFragment) getSupportFragmentManager().findFragmentByTag(CreateFragment.class.getSimpleName())).setLocation(selectedLatLng);
            }
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
            if (fragment instanceof CreateFragment) {
                DialogHelper.showConfirmDialog(this, null, getString(R.string.exit_without_save),
                        (dialogInterface, i) -> {
                            finish();
                        }, null);
            } else {
                onBackPressed();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setTitle(R.string.create);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment instanceof CreateFragment) {
            DialogHelper.showConfirmDialog(this, null, getString(R.string.exit_without_save),
                    (dialogInterface, i) -> {
                        super.onBackPressed();
                    }, null);
        } else {
            selectedLatLng = null;
            super.onBackPressed();
        }
    }

    private void save() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment != null) {
            ((CreateFragment) fragment).saveModel(selectedLatLng);
        }
    }

    @Override
    public void astroRecordSaved(AstroRecord astroRecord) {
        Intent intent = new Intent();
        intent.putExtra(CreateFragment.KEY_ASTRO_RECORD, astroRecord);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void showMap(double latitude, double longitude) {
        double finalLatitude = DEFAULT_LATITUDE;
        double finalLongitude = DEFAULT_LONGITUDE;
        if (latitude != 0 && longitude != 0) {
            finalLatitude = latitude;
            finalLongitude = longitude;
        }
        GoogleMapOptions googleMapOptions = new GoogleMapOptions()
                .camera(new CameraPosition.Builder()
                        .target(new LatLng(finalLatitude, finalLongitude))
                        .zoom(12)
                        .build());
        SupportMapFragment supportMapFragment = SupportMapFragment.newInstance(googleMapOptions);
        addFragment(R.id.content, supportMapFragment, true);
        supportMapFragment.getMapAsync(googleMap -> {
            if (latitude != 0 && longitude != 0) {
                selectedLatLng = new LatLng(latitude, longitude);
                googleMap.clear();
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(selectedLatLng);
                googleMap.addMarker(markerOptions);
            }
            //click
            googleMap.setOnMapClickListener(latLng -> {
                selectedLatLng = latLng;
                googleMap.clear();
                MarkerOptions options = new MarkerOptions()
                        .position(latLng);
                googleMap.addMarker(options);

            });
        });
        setTitle(R.string.place);
    }
}
