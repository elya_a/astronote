package com.aef.android.astronote.presentation.tools.adapter;

import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.aef.android.astronote.data.model.Ocular;
import com.aef.android.astronote.databinding.LayoutOcularItemBinding;

public class OcularViewHolder extends ToolViewHolder<Ocular> {

    private LayoutOcularItemBinding binding;

    public OcularViewHolder(View itemView, ToolItemCallback<Ocular> toolItemCallback) {
        super(itemView, toolItemCallback);
        binding = DataBindingUtil.bind(itemView);
    }

    @Override
    public void setData(Ocular data) {
        super.setData(data);
        binding.setOcular(data);
    }
}
