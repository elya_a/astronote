package com.aef.android.astronote.base

import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.app.ActionBar
import androidx.core.content.ContextCompat

import com.aef.android.astronote.App
import com.aef.android.astronote.R
import com.google.android.material.floatingactionbutton.FloatingActionButton

abstract class BaseThemedActivity : BaseActivity() {

    protected var isDarkTheme: Boolean = false
    private var themeMenuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isDarkTheme = App.getPreferenceHelper().idDarkTheme()
        if (isDarkTheme) {
            setTheme(R.style.ActivityDarkTheme)
        } else {
            setTheme(R.style.ActivityTheme)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_themed, menu)
        themeMenuItem = menu.findItem(R.id.action_theme)
        val drawable = themeMenuItem!!.icon
        initDrawableColor(drawable, R.color.colorPrimaryDark)
        return super.onCreateOptionsMenu(menu)
    }

    protected fun initDrawableColor(drawable: Drawable, @ColorRes defaultColor: Int) {
        drawable.mutate()
        if (isDarkTheme) {
            drawable.setColorFilter(ContextCompat.getColor(this, R.color.red), PorterDuff.Mode.SRC_ATOP)
        } else {
            drawable.setColorFilter(ContextCompat.getColor(this, defaultColor), PorterDuff.Mode.SRC_ATOP)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_theme -> {
                App.getPreferenceHelper().setDarkTheme(!isDarkTheme)
                recreate()
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    protected fun initDarkThemeFab(fab: FloatingActionButton, @DrawableRes drawableId: Int) {
        if (isDarkTheme) {
            fab.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorPrimary_Dark))
            val drawable = ContextCompat.getDrawable(this, drawableId)!!.mutate()
            drawable.setColorFilter(ContextCompat.getColor(this, R.color.red), PorterDuff.Mode.SRC_ATOP)
            fab.setImageDrawable(drawable)
        }
    }

    protected fun setHasBackButton() {
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
            val backDrawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp)!!.mutate()
            if (isDarkTheme) {
                backDrawable.setColorFilter(ContextCompat.getColor(this, R.color.red), PorterDuff.Mode.SRC_ATOP)
            }
            actionBar.setHomeAsUpIndicator(backDrawable)
        }
    }

    protected fun setHasBackButton(@DrawableRes drawableRes: Int) {
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
            val backDrawable = ContextCompat.getDrawable(this, drawableRes)!!.mutate()
            if (isDarkTheme) {
                backDrawable.setColorFilter(ContextCompat.getColor(this, R.color.red), PorterDuff.Mode.SRC_ATOP)
            }
            actionBar.setHomeAsUpIndicator(backDrawable)
        }
    }
}
