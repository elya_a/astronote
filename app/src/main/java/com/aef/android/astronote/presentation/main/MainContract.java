package com.aef.android.astronote.presentation.main;

import com.aef.android.astronote.data.model.AstroRecord;

import java.util.ArrayList;

class MainContract {

    interface View extends com.aef.android.astronote.base.View<Presenter> {

        void showAstroRecords(ArrayList<AstroRecord> astroRecords);

        void onError();
    }

    interface Presenter extends com.aef.android.astronote.base.Presenter {

        void loadAstroRecords(boolean isPlanned);
    }

}
