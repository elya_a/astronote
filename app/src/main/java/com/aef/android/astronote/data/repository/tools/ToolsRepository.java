package com.aef.android.astronote.data.repository.tools;

import com.aef.android.astronote.data.database.BarlowLensDao;
import com.aef.android.astronote.data.database.BarlowLensTableModel;
import com.aef.android.astronote.data.database.CameraDao;
import com.aef.android.astronote.data.database.CameraTableModel;
import com.aef.android.astronote.data.database.FocusGearDao;
import com.aef.android.astronote.data.database.FocusGearTableModel;
import com.aef.android.astronote.data.database.MatrixDao;
import com.aef.android.astronote.data.database.MatrixTableModel;
import com.aef.android.astronote.data.database.OcularDao;
import com.aef.android.astronote.data.database.OcularTableModel;
import com.aef.android.astronote.data.database.TelescopeDao;
import com.aef.android.astronote.data.database.TelescopeTableModel;
import com.aef.android.astronote.data.database.ToolDaoFabric;
import com.aef.android.astronote.data.model.BarlowLens;
import com.aef.android.astronote.data.model.CCDMatrix;
import com.aef.android.astronote.data.model.FocusGear;
import com.aef.android.astronote.data.model.Ocular;
import com.aef.android.astronote.data.model.PhotoCamera;
import com.aef.android.astronote.data.model.Telescope;
import com.aef.android.astronote.data.model.Tool;
import com.aef.android.astronote.data.repository.BaseRepository;
import com.google.gson.Gson;
import com.j256.ormlite.dao.BaseDaoImpl;

import java.sql.SQLException;
import java.util.ArrayList;

import io.reactivex.Observable;

public class ToolsRepository extends BaseRepository {

    public static final int DEFAULT_ID = -1;
    private Gson gson;

    public ToolsRepository() {
        gson = new Gson();
    }

    public Observable<ArrayList<PhotoCamera>> getCameraList() {
        return Observable.create(subscriber -> {
            ArrayList<PhotoCamera> photoCameras = new ArrayList<PhotoCamera>();
            try {
                for (CameraTableModel tableModel : dataBaseHelper.getCameraDao().queryForAll()) {
                    photoCameras.add(new PhotoCamera(tableModel));
                }
                subscriber.onNext(photoCameras);
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });
    }

    public Observable<ArrayList<Telescope>> getTelescopeList() {
        return Observable.create(subscriber -> {
            ArrayList<Telescope> telescopes = new ArrayList<Telescope>();
            try {
                for (TelescopeTableModel tableModel : dataBaseHelper.getTelescopeDao().queryForAll()) {
                    telescopes.add(new Telescope(tableModel));
                }
                subscriber.onNext(telescopes);
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });
    }

    public Observable<ArrayList<CCDMatrix>> getMatrixList() {
        return Observable.create(subscriber -> {
            ArrayList<CCDMatrix> matrixArrayList = new ArrayList<CCDMatrix>();
            try {
                for (MatrixTableModel tableModel : dataBaseHelper.getMatrixDao().queryForAll()) {
                    matrixArrayList.add(new CCDMatrix(tableModel));
                }
                subscriber.onNext(matrixArrayList);
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });
    }

    public Observable<ArrayList<Ocular>> getOcularList() {
        return Observable.create(subscriber -> {
            ArrayList<Ocular> ocularArrayList = new ArrayList<>();
            try {
                for (OcularTableModel tableModel : dataBaseHelper.getOcularDao().queryForAll()) {
                    ocularArrayList.add(new Ocular(tableModel));
                }
                subscriber.onNext(ocularArrayList);
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });
    }

    public Observable<ArrayList<BarlowLens>> getBarlowLensList() {
        return Observable.create(subscriber -> {
            ArrayList<BarlowLens> barlowLensArrayList = new ArrayList<>();
            try {
                for (BarlowLensTableModel tableModel : dataBaseHelper.getBarlowLensDao().queryForAll()) {
                    barlowLensArrayList.add(new BarlowLens(tableModel));
                }
                subscriber.onNext(barlowLensArrayList);
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });
    }

    public Observable<ArrayList<FocusGear>> getFocusGearList() {
        return Observable.create(subscriber -> {
            ArrayList<FocusGear> focusGearArrayList = new ArrayList<>();
            try {
                for (FocusGearTableModel tableModel : dataBaseHelper.getFocusGearDao().queryForAll()) {
                    focusGearArrayList.add(new FocusGear(tableModel));
                }
                subscriber.onNext(focusGearArrayList);
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });
    }

    public Observable<PhotoCamera> saveCamera(PhotoCamera camera) {
        return Observable.create(subscriber -> {
            CameraTableModel tableModel = new CameraTableModel(camera);
            try {
                dataBaseHelper.getCameraDao().createOrUpdate(tableModel);
                subscriber.onNext(new PhotoCamera(tableModel));
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }

        });
    }

    public Observable<Telescope> saveTelescope(Telescope telescope) {
        return Observable.create(subscriber -> {
            TelescopeTableModel tableModel = new TelescopeTableModel(telescope);
            try {
                dataBaseHelper.getTelescopeDao().createOrUpdate(tableModel);
                subscriber.onNext(new Telescope(tableModel));
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }

        });
    }

    public Observable<CCDMatrix> saveMatrix(CCDMatrix matrix) {
        return Observable.create(subscriber -> {
            MatrixTableModel tableModel = new MatrixTableModel(matrix);
            try {
                dataBaseHelper.getMatrixDao().createOrUpdate(tableModel);
                subscriber.onNext(new CCDMatrix(tableModel));
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }

        });
    }

    public Observable<Ocular> saveOcular(Ocular ocular) {
        return Observable.create(subscriber -> {
            OcularTableModel tableModel = new OcularTableModel(ocular);
            try {
                dataBaseHelper.getOcularDao().createOrUpdate(tableModel);
                subscriber.onNext(new Ocular(tableModel));
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }

        });
    }

    public Observable<BarlowLens> saveBarlowLens(BarlowLens barlowLens) {
        return Observable.create(subscriber -> {
            BarlowLensTableModel tableModel = new BarlowLensTableModel(barlowLens);
            try {
                dataBaseHelper.getBarlowLensDao().createOrUpdate(tableModel);
                subscriber.onNext(new BarlowLens(tableModel));
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }

        });
    }

    public Observable<FocusGear> saveFocusGear(FocusGear focusGear) {
        return Observable.create(subscriber -> {
            FocusGearTableModel tableModel = new FocusGearTableModel(focusGear);
            try {
                dataBaseHelper.getFocusGearDao().createOrUpdate(tableModel);
                subscriber.onNext(new FocusGear(tableModel));
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }

        });
    }

    public ArrayList<TelescopeTableModel> getTelescopesByJsons(ArrayList<String> telescopeJsons) throws SQLException {
        TelescopeDao telescopeDao = dataBaseHelper.getTelescopeDao();
        ArrayList<TelescopeTableModel> telescopeTableModels = new ArrayList<>();
        for (String json : telescopeJsons) {
            Telescope telescope = gson.fromJson(json, Telescope.class);
            TelescopeTableModel tableModel = telescopeDao.getTableModel(telescope.getId());
            if (tableModel == null) {
                telescope.setId(DEFAULT_ID);
                tableModel = new TelescopeTableModel(telescope);
            }
            telescopeTableModels.add(tableModel);
        }
        return telescopeTableModels;
    }

    public ArrayList<CameraTableModel> getCamerasByJsons(ArrayList<String> cameraJsons) throws SQLException {
        CameraDao cameraDao = dataBaseHelper.getCameraDao();
        ArrayList<CameraTableModel> tableModels = new ArrayList<>();
        for (String json :cameraJsons) {
            PhotoCamera camera = gson.fromJson(json, PhotoCamera.class);
            CameraTableModel tableModel = cameraDao.getTableModel(camera.getId());
            if (tableModel == null) {
                camera.setId(DEFAULT_ID);
                tableModel = new CameraTableModel(camera);
            }
            tableModels.add(tableModel);

        }
        return tableModels;
    }

    public ArrayList<MatrixTableModel> getMatrixesByJsons(ArrayList<String> matrixJsons) throws SQLException {
        MatrixDao matrixDao = dataBaseHelper.getMatrixDao();
        ArrayList<MatrixTableModel> tableModels = new ArrayList<>();
        for (String json : matrixJsons) {
            CCDMatrix matrix = gson.fromJson(json, CCDMatrix.class);
            MatrixTableModel tableModel = matrixDao.getTableModel(matrix.getId());
            if (tableModel == null) {
                matrix.setId(DEFAULT_ID);
                tableModel = new MatrixTableModel(matrix);
            }
            tableModels.add(tableModel);
        }
        return tableModels;
    }

    public ArrayList<OcularTableModel> getOcularsByJsons(ArrayList<String> Jsons) throws SQLException {
        OcularDao dao = dataBaseHelper.getOcularDao();
        ArrayList<OcularTableModel> tableModels = new ArrayList<>();
        for (String json : Jsons) {
            Ocular ocular = gson.fromJson(json, Ocular.class);
            OcularTableModel tableModel = dao.getTableModel(ocular.getId());
            if (tableModel == null) {
                ocular.setId(DEFAULT_ID);
                tableModel = new OcularTableModel(ocular);
            }
            tableModels.add(tableModel);
        }
        return tableModels;
    }

    public ArrayList<BarlowLensTableModel> getBarlowLensByJsons(ArrayList<String> Jsons) throws SQLException {
        BarlowLensDao dao = dataBaseHelper.getBarlowLensDao();
        ArrayList<BarlowLensTableModel> tableModels = new ArrayList<>();
        for (String json : Jsons) {
            BarlowLens barlowLens = gson.fromJson(json, BarlowLens.class);
            BarlowLensTableModel tableModel = dao.getTableModel(barlowLens.getId());
            if (tableModel == null) {
                barlowLens.setId(DEFAULT_ID);
                tableModel = new BarlowLensTableModel(barlowLens);
            }
            tableModels.add(tableModel);
        }
        return tableModels;
    }

    public ArrayList<FocusGearTableModel> getFocusGearsByJsons(ArrayList<String> Jsons) throws SQLException {
        FocusGearDao dao = dataBaseHelper.getFocusGearDao();
        ArrayList<FocusGearTableModel> tableModels = new ArrayList<>();
        for (String json : Jsons) {
            FocusGear focusGear = gson.fromJson(json, FocusGear.class);
            FocusGearTableModel tableModel = dao.getTableModel(focusGear.getId());
            if (tableModel == null) {
                focusGear.setId(DEFAULT_ID);
                tableModel = new FocusGearTableModel(focusGear);
            }
            tableModels.add(tableModel);
        }
        return tableModels;
    }

    public Observable<Tool> deleteTool(Tool tool) {
        return Observable.create(subscriber -> {
            try {
                if (tool instanceof Telescope) {
                    dataBaseHelper.getTelescopeDao().delete(new TelescopeTableModel((Telescope) tool));
                    subscriber.onNext(tool);
                } else if (tool instanceof PhotoCamera) {
                    dataBaseHelper.getCameraDao().delete(new CameraTableModel((PhotoCamera) tool));
                    subscriber.onNext(tool);
                } else if (tool instanceof CCDMatrix) {
                    dataBaseHelper.getMatrixDao().delete(new MatrixTableModel((CCDMatrix) tool));
                    subscriber.onNext(tool);
                } else if (tool instanceof Ocular) {
                    dataBaseHelper.getOcularDao().delete(new OcularTableModel((Ocular) tool));
                    subscriber.onNext(tool);
                } else if (tool instanceof FocusGear) {
                    dataBaseHelper.getFocusGearDao().delete(new FocusGearTableModel((FocusGear) tool));
                    subscriber.onNext(tool);
                } else if (tool instanceof BarlowLens) {
                    dataBaseHelper.getBarlowLensDao().delete(new BarlowLensTableModel((BarlowLens) tool));
                    subscriber.onNext(tool);
                }
                subscriber.onComplete();
            } catch (SQLException e) {
                subscriber.onError(e);
            }

        });

    }
}
