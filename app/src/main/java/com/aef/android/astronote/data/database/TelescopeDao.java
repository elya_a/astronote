package com.aef.android.astronote.data.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class TelescopeDao extends BaseDao<TelescopeTableModel> {

    protected TelescopeDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, TelescopeTableModel.class);
    }

}
