package com.aef.android.astronote.presentation.choose_tool.adapter;

import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.aef.android.astronote.data.model.Tool;
import com.aef.android.astronote.databinding.LayoutShortToolItemBinding;
import com.aef.android.astronote.presentation.tools.adapter.ToolViewHolder;

import java.util.ArrayList;

public class ChooseToolViewHolder<T extends Tool> extends ToolViewHolder<T> {

    LayoutShortToolItemBinding binding;
    private ArrayList<T> selectedToolList;

    public ChooseToolViewHolder(View itemView, ArrayList<T> selectedToolList, boolean isDarkTheme) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
        binding.setIsDark(isDarkTheme);
        this.selectedToolList = selectedToolList;
    }

    @Override
    public void setData(T data) {
        binding.setTitle(data.getTitle());
        checkSelection(data);
        itemView.setOnClickListener(view -> {
            if (!selectedToolList.isEmpty() && selectedToolList.contains(data)) {
                selectedToolList.remove(data);
            } else {
                selectedToolList.add(data);
            }
            checkSelection(data);
        });
    }

    private void checkSelection(T data) {
        if (!selectedToolList.isEmpty() && selectedToolList.contains(data)) {
            itemView.setSelected(true);
            binding.setSelect(true);
        } else {
            itemView.setSelected(false);
            binding.setSelect(false);
        }
    }
}
