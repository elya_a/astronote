package com.aef.android.astronote.presentation.tools.adapter;


import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.aef.android.astronote.data.model.FocusGear;
import com.aef.android.astronote.databinding.LayoutFocusItemBinding;

public class FocusGearViewHolder extends ToolViewHolder<FocusGear> {

    private LayoutFocusItemBinding binding;

    public FocusGearViewHolder(View itemView, ToolItemCallback<FocusGear> toolItemCallback) {
        super(itemView, toolItemCallback);
        binding = DataBindingUtil.bind(itemView);
    }

    @Override
    public void setData(FocusGear data) {
        super.setData(data);
        binding.setFocusGear(data);
    }
}
