package com.aef.android.astronote.data.model

import android.os.Parcel
import android.os.Parcelable

import com.aef.android.astronote.data.database.CameraTableModel

data class PhotoCamera(
    var id: Int = 0,
    var name: String? = null,
    var megaPixelCount: Double = 0.toDouble(),
    var sensorSize: String? = null,
    var cropFactor: Double = 0.toDouble()
) : Tool(ToolType.CAMERA), Parcelable {

    override val title: String
        get() = name ?: ""

    constructor(tableModel: CameraTableModel) : this(
        tableModel.id,
        tableModel.name,
        tableModel.megaPixelCount,
        tableModel.sensorSize,
        tableModel.cropFactor
    )

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readDouble()
    )

    override fun equals(other: Any?) =
        (other as? PhotoCamera)?.id == id

    override fun hashCode() = id

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(this.id)
        dest.writeString(this.name)
        dest.writeDouble(this.megaPixelCount)
        dest.writeString(this.sensorSize)
        dest.writeDouble(this.cropFactor)
    }

    override fun describeContents() = 0

    companion object {

        @JvmField
        @Suppress("unused")
        val CREATOR: Parcelable.Creator<PhotoCamera> = object : Parcelable.Creator<PhotoCamera> {
            override fun createFromParcel(source: Parcel) = PhotoCamera(source)
            override fun newArray(size: Int) = arrayOfNulls<PhotoCamera>(size)
        }
    }
}
