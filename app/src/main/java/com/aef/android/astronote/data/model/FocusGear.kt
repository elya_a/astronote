package com.aef.android.astronote.data.model

import android.os.Parcel
import android.os.Parcelable

import com.aef.android.astronote.data.database.FocusGearTableModel

import java.util.Locale

data class FocusGear(
    var id: Int = 0,
    var multiplicity: Double = 0.0
) : Tool(ToolType.FOCUS_GEAR), Parcelable {

    override val title: String
        get() = String.format(Locale.getDefault(), "%.1fx", multiplicity)

    constructor(tableModel: FocusGearTableModel) : this(
        tableModel.id,
        tableModel.multiplicity
    )

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readDouble()
    )

    override fun equals(other: Any?) =
        (other as? FocusGear)?.id == id

    override fun hashCode() = id

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(this.id)
        dest.writeDouble(this.multiplicity)
    }

    override fun describeContents() = 0

    companion object {

        @JvmField
        @Suppress("unused")
        val CREATOR: Parcelable.Creator<FocusGear> = object : Parcelable.Creator<FocusGear> {
            override fun createFromParcel(source: Parcel) = FocusGear(source)
            override fun newArray(size: Int) = arrayOfNulls<FocusGear>(size)
        }
    }
}
