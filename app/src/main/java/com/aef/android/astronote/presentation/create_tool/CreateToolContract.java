package com.aef.android.astronote.presentation.create_tool;

import com.aef.android.astronote.data.model.BarlowLens;
import com.aef.android.astronote.data.model.CCDMatrix;
import com.aef.android.astronote.data.model.FocusGear;
import com.aef.android.astronote.data.model.Ocular;
import com.aef.android.astronote.data.model.PhotoCamera;
import com.aef.android.astronote.data.model.Telescope;
import com.aef.android.astronote.data.model.Tool;

public class CreateToolContract {
    public interface View extends com.aef.android.astronote.base.View<Presenter> {

        void toolSaved(Tool tool);
    }

    public interface Presenter extends com.aef.android.astronote.base.Presenter {

        void saveCamera(PhotoCamera camera);

        void saveTelescope(Telescope telescope);

        void saveMatrix(CCDMatrix matrix);

        void saveOcular(Ocular ocular);

        void saveBarlowLens(BarlowLens barlowLens);

        void saveFocusGear(FocusGear focusGear);
    }
}
