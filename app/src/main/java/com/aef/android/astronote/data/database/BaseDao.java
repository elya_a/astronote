package com.aef.android.astronote.data.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public abstract class BaseDao<T> extends BaseDaoImpl<T, Integer> {

    protected BaseDao(ConnectionSource connectionSource, Class<T> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public T getTableModel(int id) throws SQLException {
        return queryBuilder()
                .where()
                .idEq(id)
                .queryForFirst();
    }
}
