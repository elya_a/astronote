package com.aef.android.astronote.data.database;

import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class BarlowLensDao extends BaseDao<BarlowLensTableModel> {

    protected BarlowLensDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, BarlowLensTableModel.class);
    }
}
