package com.aef.android.astronote.presentation.create.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView

import com.aef.android.astronote.R

import java.util.ArrayList

class ChipsAdapter<T : ChipItem> : RecyclerView.Adapter<ChipViewHolder<T>> {

    private var chipsList: List<T>? = null
    private var chipsListener: ChipsListener<T>? = null

    constructor() {
        this.chipsList = ArrayList()
    }

    constructor(chipsList: List<T>) {
        this.chipsList = chipsList
    }

    fun setChipsList(chipsList: List<T>) {
        this.chipsList = chipsList
        notifyDataSetChanged()
    }

    fun setChipsListener(chipsListener: ChipsListener<T>) {
        this.chipsListener = chipsListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChipViewHolder<T> {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_chip_item, parent, false)
        return ChipViewHolder(view, chipsListener)
    }

    override fun onBindViewHolder(holder: ChipViewHolder<T>, position: Int) {
        chipsList?.let {
            holder.setData(it[position])
        }
    }

    override fun getItemCount(): Int {
        return chipsList!!.size
    }

}
