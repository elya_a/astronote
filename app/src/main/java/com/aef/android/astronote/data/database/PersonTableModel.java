package com.aef.android.astronote.data.database;

import com.aef.android.astronote.data.model.Person;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import static com.aef.android.astronote.data.database.PersonTableModel.PERSON_TABLE_NAME;

@DatabaseTable(tableName = PERSON_TABLE_NAME)
public class PersonTableModel {

    public static final String PERSON_TABLE_NAME = "persons";

    @DatabaseField(id = true)
    private String name;

    public PersonTableModel() {
    }

    public PersonTableModel(String name) {
        this.name = name;
    }

    public PersonTableModel(Person person) {
        this.name = person.getName();
    }

    public String getName() {
        return name;
    }
}
