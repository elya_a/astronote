package com.aef.android.astronote.presentation.tools

import com.aef.android.astronote.base.BasePresenter
import com.aef.android.astronote.data.model.Tool
import com.aef.android.astronote.data.model.ToolType
import com.aef.android.astronote.data.repository.tools.BaseToolRepository
import com.aef.android.astronote.data.repository.tools.ToolRepositoryFactory
import com.aef.android.astronote.data.repository.tools.ToolsRepository

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ToolListPresenter(
    type: ToolType,
    toolRepositoryFactory: ToolRepositoryFactory
) : BasePresenter(), ToolListContract.ToolListPresenter {

    private var view: ToolListContract.ToolListView? = null
    private val toolRepository: BaseToolRepository<*> =
        toolRepositoryFactory.getToolRepository(type) as BaseToolRepository<*>

    override fun attach(view: ToolListContract.ToolListView) {
        this.view = view
    }

    override fun loadTools() {
        toolRepository.getToolList()
            .addSubscription(
                { view?.showToolList(it) }
            )
    }

    override fun deleteTool(tool: Tool) {
        toolRepository.deleteTool(tool)
            .addSubscription()
    }

    override fun detach() {
        onDestroy()
        view = null
    }
}
