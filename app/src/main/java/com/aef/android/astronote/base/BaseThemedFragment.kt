package com.aef.android.astronote.base

import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat

import com.aef.android.astronote.App
import com.aef.android.astronote.R
import com.google.android.material.floatingactionbutton.FloatingActionButton

open class BaseThemedFragment : BaseFragment() {

    protected var isDarkTheme: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isDarkTheme = App.getPreferenceHelper().idDarkTheme()
    }

    protected fun initDarkThemeFab(fab: FloatingActionButton, @DrawableRes drawableId: Int) {
        if (isDarkTheme) {
            fab.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(activity!!, R.color.colorPrimary_Dark))
            val drawable = ContextCompat.getDrawable(activity!!, drawableId)!!.mutate()
            drawable.setColorFilter(ContextCompat.getColor(activity!!, R.color.red), PorterDuff.Mode.SRC_ATOP)
            fab.setImageDrawable(drawable)
        }
    }

}
