package com.aef.android.astronote.data.model

import android.os.Parcel
import android.os.Parcelable

import com.aef.android.astronote.data.database.OcularTableModel

import java.util.Locale

data class Ocular(
    var id: Int = 0,
    var focus: Double = 0.0,
    //поле зрения
    var viewField: Double = 0.0
) : Tool(ToolType.OCULAR), Parcelable {

    override val title: String
        get() = String.format(Locale.getDefault(), "%.1f мм, %.1f °", focus, viewField)

    constructor(tableModel: OcularTableModel) : this(
        tableModel.id,
        tableModel.focus,
        tableModel.viewField
    )

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readDouble(),
        parcel.readDouble()
    )

    override fun equals(other: Any?) =
        (other as? Ocular)?.id == id

    override fun hashCode() = id

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(this.id)
        dest.writeDouble(this.focus)
        dest.writeDouble(this.viewField)
    }

    override fun describeContents() = 0

    companion object {

        @JvmField
        @Suppress("unused")
        val CREATOR: Parcelable.Creator<Ocular> = object : Parcelable.Creator<Ocular> {
            override fun createFromParcel(source: Parcel) = Ocular(source)
            override fun newArray(size: Int) = arrayOfNulls<Ocular>(size)
        }
    }
}
