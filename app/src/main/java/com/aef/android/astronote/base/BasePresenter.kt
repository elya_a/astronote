package com.aef.android.astronote.base

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

abstract class BasePresenter(
    private val mainThreadScheduler: Scheduler = AndroidSchedulers.mainThread(),
    private val workThreadScheduler: Scheduler = Schedulers.io()
) {

    private val compositeDisposable = CompositeDisposable()

    protected fun <T> Observable<T>.addSubscription(
        onNext: ((T) -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null,
        onComplete: (() -> Unit)? = null
    ): Disposable =
        subscribeOn(workThreadScheduler)
            .observeOn(mainThreadScheduler)
            .subscribe(
                { onNext?.invoke(it) },
                {
                    it.printStackTrace()
                    onError?.invoke(it)
                },
                { onComplete?.invoke() }
            )
            .also {
                compositeDisposable.add(it)
            }

    protected fun onDestroy() {
        compositeDisposable.dispose()
    }
}