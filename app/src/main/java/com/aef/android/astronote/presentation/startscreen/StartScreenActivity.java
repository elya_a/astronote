package com.aef.android.astronote.presentation.startscreen;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.aef.android.astronote.base.BaseActivity;
import com.aef.android.astronote.presentation.main.MainActivity;

public class StartScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.start(this);
        finish();
    }
}
