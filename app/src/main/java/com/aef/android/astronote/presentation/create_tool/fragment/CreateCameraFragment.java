package com.aef.android.astronote.presentation.create_tool.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.aef.android.astronote.R;
import com.aef.android.astronote.data.model.PhotoCamera;

import butterknife.BindView;

public class CreateCameraFragment extends BaseCreateToolFragment<PhotoCamera> {

    @BindView(R.id.name_et)
    EditText nameEditText;
    @BindView(R.id.mpixels_et)
    EditText mpixelsEditText;
    @BindView(R.id.sensor_et)
    EditText sensorEditText;
    @BindView(R.id.crop_et)
    EditText cropEditText;

    public static CreateCameraFragment newInstance() {
        return new CreateCameraFragment();
    }

    public static CreateCameraFragment newInstance(PhotoCamera tool) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_TOOL, tool);
        CreateCameraFragment fragment = new CreateCameraFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_create_camera;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    protected PhotoCamera newTool() {
        return new PhotoCamera();
    }

    @Override
    protected void initFields(PhotoCamera tool) {
        nameEditText.setText(tool.getName());
        mpixelsEditText.setText(String.valueOf(tool.getMegaPixelCount()));
        sensorEditText.setText(tool.getSensorSize());
        cropEditText.setText(String.valueOf(tool.getCropFactor()));
    }

    @Override
    protected boolean isValid() {
        return !(TextUtils.isEmpty(nameEditText.getText()));
    }

    @Override
    public PhotoCamera getToolModel() {
        PhotoCamera photoCamera = tool;
        photoCamera.setName(nameEditText.getText().toString());
        if (!TextUtils.isEmpty(mpixelsEditText.getText().toString())) {
            photoCamera.setMegaPixelCount(Double.valueOf(mpixelsEditText.getText().toString()));
        }
        photoCamera.setSensorSize(sensorEditText.getText().toString());
        if (!TextUtils.isEmpty(cropEditText.getText().toString())) {
            photoCamera.setCropFactor(Double.valueOf(cropEditText.getText().toString()));
        }
        return photoCamera;
    }
}
