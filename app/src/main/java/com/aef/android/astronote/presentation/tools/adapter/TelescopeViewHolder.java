package com.aef.android.astronote.presentation.tools.adapter;

import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.aef.android.astronote.data.model.Telescope;
import com.aef.android.astronote.databinding.LayoutTelescopeItemBinding;

public class TelescopeViewHolder extends ToolViewHolder<Telescope> {

    LayoutTelescopeItemBinding binding;

    public TelescopeViewHolder(View itemView, ToolItemCallback<Telescope> toolItemCallback) {
        super(itemView, toolItemCallback);
        binding = DataBindingUtil.bind(itemView);
    }

    public void setData(Telescope telescope) {
        super.setData(telescope);
        binding.setIsExpand(false);
        binding.setTelescope(telescope);
        binding.setHolder(this);
        binding.setIsDark(isDark);
    }

    public void setExpanded(boolean expanded) {
        binding.setIsExpand(expanded);
    }
}
