package com.aef.android.astronote.presentation.create;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.aef.android.astronote.R;
import com.aef.android.astronote.base.BaseInteractionFragment;
import com.aef.android.astronote.data.model.AstroObject;
import com.aef.android.astronote.data.model.AstroRecord;
import com.aef.android.astronote.data.model.BarlowLens;
import com.aef.android.astronote.data.model.CCDMatrix;
import com.aef.android.astronote.data.model.FocusGear;
import com.aef.android.astronote.data.model.Ocular;
import com.aef.android.astronote.data.model.Person;
import com.aef.android.astronote.data.model.PhotoCamera;
import com.aef.android.astronote.data.model.Telescope;
import com.aef.android.astronote.data.model.Tool;
import com.aef.android.astronote.databinding.FragmentCreateBinding;
import com.aef.android.astronote.presentation.choose_tool.ChooseToolActivity;
import com.aef.android.astronote.presentation.choose_tool.ChooseToolFragment;
import com.aef.android.astronote.presentation.create.adapter.ChipsAdapter;
import com.aef.android.astronote.utils.DateFormatHelper;
import com.aef.android.astronote.utils.DialogHelper;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CreateFragment extends BaseInteractionFragment<CreateFragmentInteraction> implements CreateContract.View {
    private static final String TAG = CreateFragment.class.getSimpleName();
    public static final String KEY_ASTRO_RECORD = "KEY_ASTRO_RECORD";
    private static final int CHOOSE_TOOLS_REQUEST_CODE = 1;

    @BindView(R.id.start_date_tv)
    TextView startDateTextView;
    @BindView(R.id.start_time_tv)
    TextView startTimeTextView;
    @BindView(R.id.end_date_tv)
    TextView endDateTextView;
    @BindView(R.id.end_time_tv)
    TextView endTimeTextView;
    @BindView(R.id.choose_place_tv)
    TextView placeTextView;
    @BindView(R.id.weather_et)
    EditText weatherEditText;
    @BindView(R.id.pressure_et)
    EditText pressureEditText;
    @BindView(R.id.temp_et)
    EditText tempEditText;
    @BindView(R.id.telescopes_rv)
    RecyclerView telescopesRv;
    Unbinder unbinder;
    @BindView(R.id.cameras_rv)
    RecyclerView camerasRv;
    @BindView(R.id.matrixes_rv)
    RecyclerView matrixesRv;
    @BindView(R.id.people_rv)
    RecyclerView peopleRv;
    @BindView(R.id.objects_rv)
    RecyclerView objectsRv;
    @BindView(R.id.oculars_rv)
    RecyclerView ocularRv;
    @BindView(R.id.barlow_rv)
    RecyclerView barlowRv;
    @BindView(R.id.focus_gear_rv)
    RecyclerView focusGearRv;

    private CreateContract.Presenter presenter;
    private AstroRecord astroRecord;
    private FragmentCreateBinding binding;
    //lists
    private ChipsAdapter<Telescope> telescopeChipsAdapter;
    private ChipsAdapter<PhotoCamera> cameraChipsAdapter;
    private ChipsAdapter<CCDMatrix> matrixChipsAdapter;
    private ChipsAdapter<Person> peopleChipsAdapter;
    private ChipsAdapter<AstroObject> astroObjectChipsAdapter;
    private ChipsAdapter<Ocular> ocularChipsAdapter;
    private ChipsAdapter<BarlowLens> barlowLensChipsAdapter;
    private ChipsAdapter<FocusGear> focusGearChipsAdapter;
    private ArrayList<String> personList = new ArrayList<>();

    private AlertDialog personDialog;
    private AppCompatAutoCompleteTextView personTextView;
    private InputMethodManager inputMethodManager;

    public static CreateFragment newInstance(Bundle args) {
        CreateFragment fragment = new CreateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create, container, false);
        unbinder = ButterKnife.bind(this, view);
        binding = DataBindingUtil.bind(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setPresenter(new CreatePresenter(this));
        presenter.loadPeople();

        initViews();

        Bundle args = getArguments();
        if (args != null && args.containsKey(KEY_ASTRO_RECORD)) {
            astroRecord = args.getParcelable(KEY_ASTRO_RECORD);
            initViewFields();
        }

        if (astroRecord == null) {
            astroRecord = new AstroRecord();
        }
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    private void initViews() {
        binding.setIsDark(isDarkTheme);

        SpacingItemDecoration itemDecoration = new SpacingItemDecoration(getResources().getDimensionPixelSize(R.dimen.margin_xs),
                getResources().getDimensionPixelSize(R.dimen.margin_xs));
        //telescopes
        telescopesRv.setLayoutManager(ChipsLayoutManager.newBuilder(getContext()).build());
        telescopesRv.addItemDecoration(itemDecoration);
        telescopeChipsAdapter = new ChipsAdapter<>();
        telescopesRv.setAdapter(telescopeChipsAdapter);
        //cameras
        camerasRv.setLayoutManager(ChipsLayoutManager.newBuilder(getContext()).build());
        camerasRv.addItemDecoration(itemDecoration);
        cameraChipsAdapter = new ChipsAdapter<>();
        camerasRv.setAdapter(cameraChipsAdapter);
        //matrixes
        matrixesRv.setLayoutManager(ChipsLayoutManager.newBuilder(getContext()).build());
        matrixesRv.addItemDecoration(itemDecoration);
        matrixChipsAdapter = new ChipsAdapter<>();
        matrixesRv.setAdapter(matrixChipsAdapter);
        //oculars
        ocularRv.setLayoutManager(ChipsLayoutManager.newBuilder(getContext()).build());
        ocularRv.addItemDecoration(itemDecoration);
        ocularChipsAdapter = new ChipsAdapter<>();
        ocularRv.setAdapter(ocularChipsAdapter);
        //barlow
        barlowRv.setLayoutManager(ChipsLayoutManager.newBuilder(getContext()).build());
        barlowRv.addItemDecoration(itemDecoration);
        barlowLensChipsAdapter = new ChipsAdapter<>();
        barlowRv.setAdapter(barlowLensChipsAdapter);
        //focus gear
        focusGearRv.setLayoutManager(ChipsLayoutManager.newBuilder(getContext()).build());
        focusGearRv.addItemDecoration(itemDecoration);
        focusGearChipsAdapter = new ChipsAdapter<>();
        focusGearRv.setAdapter(focusGearChipsAdapter);

        //people
        peopleRv.setLayoutManager(ChipsLayoutManager.newBuilder(getContext()).build());
        peopleRv.addItemDecoration(itemDecoration);
        peopleChipsAdapter = new ChipsAdapter<>();
        peopleChipsAdapter.setChipsListener(chip -> {
            if (astroRecord.getPeopleList() != null && astroRecord.getPeopleList().contains(chip)) {
                int pos = astroRecord.getPeopleList().indexOf(chip);
                astroRecord.getPeopleList().remove(pos);
                peopleChipsAdapter.notifyItemRemoved(pos);
                if (astroRecord.getPeopleList().isEmpty()) {
                    peopleRv.setVisibility(View.GONE);
                }
            }
        });
        peopleRv.setAdapter(peopleChipsAdapter);
        //astro objects
        objectsRv.setLayoutManager(ChipsLayoutManager.newBuilder(getContext()).build());
        objectsRv.addItemDecoration(itemDecoration);
        astroObjectChipsAdapter = new ChipsAdapter<>();
        objectsRv.setAdapter(astroObjectChipsAdapter);

        initPersonDialog();
    }

    private void initViewFields() {
        if (astroRecord != null) {
            startDateTextView.setText(DateFormatHelper.getDate(astroRecord.getStartDate()));
            startTimeTextView.setText(DateFormatHelper.getTime(astroRecord.getStartTime()));
            endDateTextView.setText(DateFormatHelper.getDate(astroRecord.getEndDate()));
            endTimeTextView.setText(DateFormatHelper.getTime(astroRecord.getEndTime()));
            weatherEditText.setText(astroRecord.getWeatherDescription());
            pressureEditText.setText(String.valueOf(astroRecord.getWeatherPressure()));
            tempEditText.setText(String.valueOf(astroRecord.getWeatherTemperature()));
            //списки
            telescopeChipsAdapter.setChipsList(astroRecord.getTelescopes());
            cameraChipsAdapter.setChipsList(astroRecord.getPhotoCameras());
            matrixChipsAdapter.setChipsList(astroRecord.getCcdMatrixList());
            ocularChipsAdapter.setChipsList(astroRecord.getOculars());
            barlowLensChipsAdapter.setChipsList(astroRecord.getBarlowLensList());
            focusGearChipsAdapter.setChipsList(astroRecord.getFocusGears());
            peopleChipsAdapter.setChipsList(astroRecord.getPeopleList());
        }
    }

    @OnClick({R.id.start_date_tv, R.id.end_date_tv})
    void onDateClick(TextView textView) {
        long initMillis = textView.getId() == R.id.start_date_tv ? astroRecord.getStartDate() : astroRecord.getEndDate();
        if (initMillis <= 0) {
            initMillis = System.currentTimeMillis();
        }
        DialogHelper.showDatePickerDialog(getActivity(), initMillis, (view, year, monthOfYear, dayOfMonth) -> {
            long millis = DateFormatHelper.getMillisDate(year, monthOfYear, dayOfMonth);
            if (textView.getId() == R.id.start_date_tv) {
                astroRecord.setStartDate(millis);
            } else {
                astroRecord.setEndDate(millis);
            }
            textView.setText(DateFormatHelper.getDate(millis));
        }, null, null);
    }

    @OnClick({R.id.start_time_tv, R.id.end_time_tv})
    void onTimeClick(TextView textView) {
        long initMillis = textView.getId() == R.id.start_time_tv ? astroRecord.getStartTime() : astroRecord.getEndTime();
        if (initMillis <= 0) {
            initMillis = System.currentTimeMillis();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(initMillis);
        DialogHelper.showTimePickerDialog(getActivity(), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                (view, hourOfDay, minute) -> {
                    long millis = DateFormatHelper.getMillisTime(hourOfDay, minute);
                    if (textView.getId() == R.id.start_time_tv) {
                        astroRecord.setStartTime(millis);
                    } else {
                        astroRecord.setEndTime(millis);
                    }
                    textView.setText(DateFormatHelper.getTime(millis));
                });
    }

    @Override
    public void setPresenter(CreateContract.Presenter presenter) {
        this.presenter = presenter;
    }

    void saveModel(LatLng selectedLatLng) {
        if (isValid()) {
            initModelFields();
            if (selectedLatLng != null) {
                astroRecord.setLatitude(selectedLatLng.latitude);
                astroRecord.setLongitude(selectedLatLng.longitude);
            }
            presenter.saveAstroRecord(astroRecord);
        } else {
            Toast.makeText(getActivity(), "Неверные значения", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.choose_place_tv)
    void changePlace() {
        fragmentInteractionCallback.showMap(astroRecord.getLatitude(), astroRecord.getLongitude());
    }

    @OnClick({R.id.telescopes_tv, R.id.cameras_tv, R.id.matrixes_tv, R.id.people_tv, R.id.objects_tv,
            R.id.oculars_tv, R.id.barlow_tv, R.id.focus_gear_tv})
    void selectView(View view) {
        switch (view.getId()) {
            case R.id.telescopes_tv:
                chooseTool(Tool.Companion.getTELESCOPE(), astroRecord.getTelescopes());
                break;
            case R.id.cameras_tv:
                chooseTool(Tool.Companion.getCAMERA(), astroRecord.getPhotoCameras());
                break;
            case R.id.matrixes_tv:
                chooseTool(Tool.Companion.getMATRIX(), astroRecord.getCcdMatrixList());
                break;
            case R.id.oculars_tv:
                chooseTool(Tool.Companion.getOCULAR(), astroRecord.getOculars());
                break;
            case R.id.barlow_tv:
                chooseTool(Tool.Companion.getBARLOW_LENS(), astroRecord.getBarlowLensList());
                break;
            case R.id.focus_gear_tv:
                chooseTool(Tool.Companion.getFOCUS_GEAR(), astroRecord.getFocusGears());
                break;
            case R.id.people_tv:
                showPersonDialog();
                break;
            case R.id.objects_tv:
                break;
        }
    }

    private void showPersonDialog() {
        personTextView.setText(null);
        personDialog.show();
        inputMethodManager.toggleSoftInputFromWindow(personTextView.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }

    private void initPersonDialog() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_person_input, null);
        personTextView = (AppCompatAutoCompleteTextView) view.findViewById(R.id.autocomplete_tv);
        personTextView.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line,
                personList));
        personDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.input_person)
                .setView(view)
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    if (!TextUtils.isEmpty(personTextView.getText().toString())) {
                        if (astroRecord.getPeopleList() == null) {
                            astroRecord.setPeopleList(new ArrayList<>());
                        }
                        astroRecord.getPeopleList().add(new Person(personTextView.getText().toString()));
                        peopleRv.setVisibility(View.VISIBLE);
                        peopleChipsAdapter.setChipsList(astroRecord.getPeopleList());
                    }
                    inputMethodManager.hideSoftInputFromWindow(personTextView.getWindowToken(), 0);
                })
                .setNeutralButton(R.string.cancel, (dialog, which) -> {
                    inputMethodManager.hideSoftInputFromWindow(personTextView.getWindowToken(), 0);
                })
                .create();
    }

    private void chooseTool(@Tool.Type int telescope, ArrayList<? extends Tool> tools) {
        ChooseToolActivity.startForResult(this, CHOOSE_TOOLS_REQUEST_CODE, telescope, tools);
    }

    private void initModelFields() {
        //date init before
        astroRecord.setWeatherDescription(weatherEditText.getText().toString());
        if (!TextUtils.isEmpty(pressureEditText.getText().toString())) {
            astroRecord.setWeatherPressure(Integer.parseInt(pressureEditText.getText().toString()));
        }
        astroRecord.setWeatherTemperature(tempEditText.getText().toString());
        //list init before
    }

    private boolean isValid() {
        return !TextUtils.isEmpty(startDateTextView.getText()) && !TextUtils.isEmpty(startTimeTextView.getText()) &&
                !TextUtils.isEmpty(endDateTextView.getText()) && !TextUtils.isEmpty(endTimeTextView.getText());
    }

    @Override
    public void recordSaved(AstroRecord astroRecord) {
        fragmentInteractionCallback.astroRecordSaved(astroRecord);
    }

    @Override
    public void onError() {
        getActivity().finish();
    }

    @Override
    public void showPeople(ArrayList<String> people) {
        personList.addAll(people);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CHOOSE_TOOLS_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    @Tool.Type int type = data.getIntExtra(ChooseToolFragment.KEY_TOOL_TYPE, Tool.Companion.getCAMERA());
                    ArrayList<? extends Tool> selectedList = data.getParcelableArrayListExtra(ChooseToolFragment.KEY_TOOL_LIST);
                    switch (type) {
                        case Tool.Companion.getBARLOW_LENS():
                            astroRecord.setBarlowLensList((ArrayList<BarlowLens>) selectedList);
                            if (selectedList.isEmpty()) {
                                barlowRv.setVisibility(View.GONE);
                            } else {
                                barlowRv.setVisibility(View.VISIBLE);
                            }
                            barlowLensChipsAdapter.setChipsList(astroRecord.getBarlowLensList());
                            break;
                        case Tool.Companion.getCAMERA():
                            astroRecord.setPhotoCameras((ArrayList<PhotoCamera>) selectedList);
                            if (selectedList.isEmpty()) {
                                camerasRv.setVisibility(View.GONE);
                            } else {
                                camerasRv.setVisibility(View.VISIBLE);
                            }
                            cameraChipsAdapter.setChipsList(astroRecord.getPhotoCameras());
                            break;
                        case Tool.Companion.getFOCUS_GEAR():
                            astroRecord.setFocusGears((ArrayList<FocusGear>) selectedList);
                            if (selectedList.isEmpty()) {
                                focusGearRv.setVisibility(View.GONE);
                            } else {
                                focusGearRv.setVisibility(View.VISIBLE);
                            }
                            focusGearChipsAdapter.setChipsList(astroRecord.getFocusGears());
                            break;
                        case Tool.Companion.getMATRIX():
                            astroRecord.setCcdMatrixList((ArrayList<CCDMatrix>) selectedList);
                            if (selectedList.isEmpty()) {
                                matrixesRv.setVisibility(View.GONE);
                            } else {
                                matrixesRv.setVisibility(View.VISIBLE);
                            }
                            matrixChipsAdapter.setChipsList(astroRecord.getCcdMatrixList());
                            break;
                        case Tool.Companion.getOCULAR():
                            astroRecord.setOculars((ArrayList<Ocular>) selectedList);
                            if (selectedList.isEmpty()) {
                                ocularRv.setVisibility(View.GONE);
                            } else {
                                ocularRv.setVisibility(View.VISIBLE);
                            }
                            ocularChipsAdapter.setChipsList(astroRecord.getOculars());
                            break;
                        case Tool.Companion.getTELESCOPE():
                            astroRecord.setTelescopes((ArrayList<Telescope>) selectedList);
                            if (selectedList.isEmpty()) {
                                telescopesRv.setVisibility(View.GONE);
                            } else {
                                telescopesRv.setVisibility(View.VISIBLE);
                            }
                            telescopeChipsAdapter.setChipsList(astroRecord.getTelescopes());
                            break;
                    }
                }
                break;
            default:

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setLocation(LatLng selectedLatLng) {
        astroRecord.setLatitude(selectedLatLng.latitude);
        astroRecord.setLongitude(selectedLatLng.longitude);
        placeTextView.setText(String.format("%f, %f", selectedLatLng.latitude, selectedLatLng.longitude));
        placeTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
    }
}
