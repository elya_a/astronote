package com.aef.android.astronote.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {
    public static final String DATABASE_NAME = "astro_note.db";
    private static final int DATABASE_VERSION = 1;

    private AstroRecordDao astroRecordDao;
    private CameraDao cameraDao;
    private TelescopeDao telescopeDao;
    private MatrixDao matrixDao;
    private OcularDao ocularDao;
    private BarlowLensDao barlowLensDao;
    private FocusGearDao focusGearDao;
    private BaseDaoImpl<PersonTableModel, String> personDao;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, AstroRecordTableModel.class);
            TableUtils.createTable(connectionSource, CameraTableModel.class);
            TableUtils.createTable(connectionSource, MatrixTableModel.class);
            TableUtils.createTable(connectionSource, TelescopeTableModel.class);
            TableUtils.createTable(connectionSource, BarlowLensTableModel.class);
            TableUtils.createTable(connectionSource, FocusGearTableModel.class);
            TableUtils.createTable(connectionSource, OcularTableModel.class);
            TableUtils.createTable(connectionSource, PersonTableModel.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    public AstroRecordDao getAstroRecordDao() throws SQLException {
        if (astroRecordDao == null) {
            astroRecordDao = new AstroRecordDao(getConnectionSource());
        }
        return astroRecordDao;
    }

    public CameraDao getCameraDao() throws SQLException {
        if (cameraDao == null) {
            cameraDao = new CameraDao(getConnectionSource());
        }
        return cameraDao;
    }

    public TelescopeDao getTelescopeDao() throws SQLException {
        if (telescopeDao == null) {
            telescopeDao = new TelescopeDao(getConnectionSource());
        }
        return telescopeDao;
    }

    public MatrixDao getMatrixDao() throws SQLException {
        if (matrixDao == null) {
            matrixDao = new MatrixDao(getConnectionSource());
        }
        return matrixDao;
    }

    public OcularDao getOcularDao() throws SQLException {
        if (ocularDao == null) {
            ocularDao = new OcularDao(getConnectionSource());
        }
        return ocularDao;
    }

    public BarlowLensDao getBarlowLensDao() throws SQLException {
        if (barlowLensDao == null) {
            barlowLensDao = new BarlowLensDao(getConnectionSource());
        }
        return barlowLensDao;
    }

    public FocusGearDao getFocusGearDao() throws SQLException {
        if (focusGearDao == null) {
            focusGearDao = new FocusGearDao(getConnectionSource());
        }
        return focusGearDao;
    }

    public BaseDaoImpl<PersonTableModel, String> getPersonDao() throws SQLException {
        if (personDao == null) {
            personDao = DaoManager.createDao(getConnectionSource(), PersonTableModel.class);
        }
        return personDao;
    }
}
